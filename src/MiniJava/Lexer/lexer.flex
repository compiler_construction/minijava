package MiniJava.Lexer;

//Author: Xu Ruiyang, Akhil Pawar

%%
%public
%class Lexer
%type Type
%column
%line

LineTerminator = \r|\n|\r\n
InputCharacter = [^\r\n]
WhiteSpace     = {LineTerminator} | [ \t\f]

/* comments */
Comment = {TraditionalComment} | {EndOfLineComment} | {DocumentationComment}

TraditionalComment   = "/*" [^*] ~"*/" | "/*" "*"+ "/"
// Comment can be the last line of the file, without line terminator.
EndOfLineComment     = "//" {InputCharacter}* {LineTerminator}?
DocumentationComment = "/**" {CommentContent} "*"+ "/"
CommentContent       = ( [^*] | \*+ [^/*] )*

Identifier = [:jletter:] [:jletterdigit:]*

DecIntegerLiteral = 0 | [1-9][0-9]*
StringLiteral = \"([^\\\"]|\\.)*\"


%%
<<EOF>> {
    return Type.EOF;
}

    /* keywords */
    <YYINITIAL> ":"            { return Type.COLON; }
    <YYINITIAL> ";"            { return Type.SEMICOLON; }
    <YYINITIAL> "."            { return Type.DOT; }
    <YYINITIAL> ","            { return Type.COMMA; }
    <YYINITIAL> "class"            { return Type.CLASS; }
    <YYINITIAL> "public"            { return Type.PUBLIC; }
    <YYINITIAL> "static"            { return Type.STATIC; }
    <YYINITIAL> "boolean"            { return Type.BOOLEAN; }
    <YYINITIAL> "true"            { return Type.TRUE; }
    <YYINITIAL> "void"            { return Type.VOID; }
    <YYINITIAL> "String"            { return Type.STRING; }
    <YYINITIAL> "extends"            { return Type.EXTENDS; }
    <YYINITIAL> "int"            { return Type.INT; }
    <YYINITIAL> "while"            { return Type.WHILE; }
    <YYINITIAL> "if"            { return Type.IF; }
    <YYINITIAL> "else"            { return Type.ELSE; }
    <YYINITIAL> "main"            { return Type.MAIN; }
    <YYINITIAL> "return"            { return Type.RETURN; }
    <YYINITIAL> "length"            { return Type.LENGTH; }
    <YYINITIAL> "false"            { return Type.FALSE; }
    <YYINITIAL> "this"            { return Type.THIS; }
    <YYINITIAL> "new"            { return Type.NEW; }
    <YYINITIAL> "System.out.println"            { return Type.PRINTLN; }
    <YYINITIAL> "sidef"            { return Type.SIDEF; }


    <YYINITIAL> {
      /* identifiers */
      {Identifier}                   { return Type.ID; }

      /* literals */
      {DecIntegerLiteral}            { return Type.INTLIT; }

      {StringLiteral}                { return Type.STRINGLIT; }

      /* operators */
      "="                            { return Type.EQSIGN; }
      "=="                           { return Type.EQUALS; }
      "+"                            { return Type.PLUS; }
      "{"                            { return Type.LBRACE; }
      "}"                            { return Type.RBRACE; }
      "("                            { return Type.LPAREN; }
      ")"                            { return Type.RPAREN; }
      "!"                            { return Type.BANG; }
      "["                            { return Type.LBRACKET; }
      "]"                            { return Type.RBRACKET; }
      "&&"                           { return Type.AND; }
      "||"                           { return Type.OR; }
      "<"                            { return Type.LESSTHAN; }
      "-"                            { return Type.MINUS; }
      "*"                            { return Type.TIMES; }
      "/"                            { return Type.DIV; }



      /* comments */
      {Comment}                      { /* ignore */ }

      /* whitespace */
      {WhiteSpace}                   { /* ignore */ }

    }

    /* error fallback */
[^]                              { return Type.BAD; }