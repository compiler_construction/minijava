package MiniJava.Lexer;
/**
 * This class is a representation of a token.
 * Author: Xu Ruiyang, Akhil Pawar
 **/
public class Token {
    private String value;   // The value for identifier and literals
    private Type tok;       // The token type
	private int row;
	private int column;

    public Token(String lvalue, Type ltok, int row, int column) {
        this.value = lvalue;
        this.tok = ltok;
        this.row = row;
        this.column = column;
    }

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}

	public String getValue() {
		return value;
	}

	public Type getType() {
		return tok;
	}

	public String toString(){
        if(this.tok == Type.INTLIT || this.tok == Type.STRINGLIT || this.tok == Type.ID){
            return this.tok + "(" + this.value + ")";
        }
        else{
            return "" + this.tok + "()";
        }
    }
}
