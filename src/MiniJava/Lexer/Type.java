package MiniJava.Lexer;

/**
 * This class is an enumeration to the token types.
 * Author: Xu Ruiyang, Akhil Pawar
 **/
public enum Type {
	BAD,
	EOF,
	COLON,
	SEMICOLON,
	DOT,
	COMMA,
	EQSIGN,
	EQUALS,
	BANG,
	LPAREN,
	RPAREN,
	LBRACKET,
	RBRACKET,
	LBRACE,
	RBRACE,
	AND,
	OR,
	LESSTHAN,
	PLUS,
	MINUS,
	TIMES,
	DIV,
	CLASS,
	PUBLIC,
	STATIC,
	VOID,
	STRING,
	EXTENDS,
	INT,
	BOOLEAN,
	WHILE,
	IF,
	ELSE,
	MAIN,
	RETURN,
	LENGTH,
	TRUE,
	FALSE,
	THIS,
	NEW,
	PRINTLN,
	SIDEF,
	ID,
	INTLIT,
	STRINGLIT
}