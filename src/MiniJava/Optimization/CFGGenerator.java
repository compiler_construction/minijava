package MiniJava.Optimization;

import MiniJava.AST.*;
import MiniJava.AST.Class;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class CFGGenerator implements Visitor<Tree> {
	private Class currentClass;
	public List<CFGGraph> graphs;
	private int current;
	private CFGNode left;
	private CFGNode right;
	private CFGNode exitNode;
	private Expression expressionRoot;
	private boolean return_on;
	private int flatten;

	public CFGGenerator() {
		graphs = new ArrayList<>();
		return_on = false;
		current = 0;
	}

	@Override
	public Tree visit(Print n) {
		return null;
	}

	@Override
	public Tree visit(Assign n) {
		expressionRoot = n.expr;
//		assignVar = n.varID;
		Expression e = (Expression)n.expr.accept(this);
		expressionRoot = null;
//		assignVar = null;
		Assign new_assign = new Assign(n.varID, e, 0, 0);
		addStatement(new_assign);
		return new_assign;
	}

	private void addStatement(Statement n) {
		CFGNode node = new CFGNode();
		node.name = graphs.get(current).nodes.size() + "";
		if (right == left) {
			for (int i = 0; i < graphs.get(current).nodes.size(); ++i) {
				CFGNode cfgNode = graphs.get(current).nodes.get(i);
				for (int j = 0; j < cfgNode.connections.size(); ++j) {
					CFGEdge edge = cfgNode.connections.get(j);
					if (edge.end.name.equals("exit")) {
						edge.end = node;
					}
				}
			}
		} else {
			//introduce a node between current node and exit node
			// remove the edge from the current node to exit
			for (int i = 0; i < left.connections.size(); ++i) {
				CFGEdge edge = left.connections.get(i);
				if (edge.end.name.equals(right.name)) {
					edge.end = node;
				}
			}
		}
		CFGEdge to_exit = new CFGEdge(node, right, n);
		node.connections.add(to_exit);
		graphs.get(current).nodes.add(node);
		left = node;
	}

	@Override
	public Tree visit(Skip n) {
		return null;
	}

	@Override
	public Tree visit(Block n) {
		for (Statement s : n.body) {
			s.accept(this);
		}
		return n;
	}

	@Override
	public Tree visit(IfThenElse n) {
		n.expr.accept(this);
		Expression expression = new Var("_t" + flatten, 0, 0);
		Yes yes = new Yes(expression, 0, 0);
		addStatement(yes);
		CFGNode tem_current = left;
		n.then.accept(this);

		left = tem_current;
		Not not = new Not(expression, 0, 0);
		CFGNode not_node = new CFGNode();
		not_node.name = graphs.get(current).nodes.size() + "";
		tem_current.connections.add(new CFGEdge(tem_current, not_node, not));
		left = not_node;
		n.elze.accept(this);

		left = right;
		return n;
	}

	@Override
	public Tree visit(While n) {
		n.expr.accept(this);
		CFGNode start = left;
		Expression expression = new Var("_t" + flatten, 0, 0);
		Yes yes = new Yes(expression, 0, 0);
		addStatement(yes);
		CFGNode yes_start = left;
		CFGNode yes_end = new CFGNode();
		yes_end.name = graphs.get(current).nodes.size() + "";
		graphs.get(current).nodes.add(yes_end);
		left.connections.clear();
		left.connections.add(new CFGEdge(left, yes_end, yes));

		Statement s1 = ((Block)(n.body)).body.get(0);
		yes_end.connections.add(new CFGEdge(yes_end, start, s1));
		Not not = new Not(expression, 0, 0);
		yes_start.connections.add(new CFGEdge(yes_start, exitNode, not));
		left = yes_end;
		right = start;
		for (int i = 1; i < ((Block)(n.body)).body.size(); ++i)
		{
			((Block)(n.body)).body.get(i).accept(this);
		}
		right = exitNode;
		left = right;
		return n;
	}

	@Override
	public Tree visit(For n) {
		return null;
	}

	@Override
	public Tree visit(Var n) {
		n.flattenVariable = n.varID;
		if (return_on) {
			addStatement(n);
		}
		return n;
	}

	@Override
	public Tree visit(IntLiteral n) {
		n.flattenVariable = n.value + "";
		if (return_on) {
			addStatement(n);
		}
		return n;
	}

	@Override
	public Tree visit(StringLiteral n) {
		n.flattenVariable = n.value;
		if (return_on) {
			addStatement(n);
		}
		return n;
	}

	@Override
	public Tree visit(Plus n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		Plus p = new Plus(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(Minus n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		Minus p = new Minus(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(Times n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		Times p = new Times(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(Division n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		Division p = new Division(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(Modulo n) {
		return null;
	}

	@Override
	public Tree visit(Equals n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		Equals p = new Equals(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(GreaterThan n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		GreaterThan p = new GreaterThan(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(LessThan n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		LessThan p = new LessThan(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(And n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		And p = new And(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(Or n) {
		n.lhs.accept(this);
		n.rhs.accept(this);
		String e = "_t" + (++flatten);
		n.flattenVariable = e;
		Var l = new Var(n.lhs.flattenVariable, 0, 0);
		Var r = new Var(n.rhs.flattenVariable, 0, 0);
		Or p = new Or(l, r, 0, 0);
		if (expressionRoot != null && expressionRoot == n) {
			return p;
		}
		Assign assign = new Assign(new Var(n.flattenVariable, 0, 0), p, 0, 0);
		addStatement(assign);
		return n;
	}

	@Override
	public Tree visit(Neg n) {
		return n;
	}

	@Override
	public Tree visit(Not n) {
		return null;
	}

	@Override
	public Tree visit(Type t) {
		return null;
	}

	@Override
	public Tree visit(Class v) {
		StringBuilder stringBuilder = new StringBuilder();
		currentClass = v;
		for (Function f : v.functions) {
			flatten = 0;
			stringBuilder.append(f.accept(this) + "\n");
			current++;
		}
		return v;
	}

	@Override
	public Tree visit(VarDecl varDecl) {
		return varDecl;
	}

	@Override
	public Tree visit(Function function) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append("digraph " + function.id.varID + " {\n");
		CFGGraph cfgGraph = new CFGGraph();

		//start node
		CFGNode entry = new CFGNode();
		entry.name = "entry";
		//end node
		CFGNode exit = new CFGNode();
		exit.name = "exit";


		cfgGraph.nodes.add(entry);
		cfgGraph.nodes.add(exit);

		left = entry;
		right = exit;
		exitNode = exit;

		graphs.add(cfgGraph);
		function.block.accept(this);
		for (CFGNode node : cfgGraph.nodes) {
			stringBuilder.append(DFSDOT(node));
		}
		stringBuilder.append("\n}");
		try {
			FileWriter writer = new FileWriter(currentClass.id.varID + "_"+ function.id.varID + ".dot");
			PrintWriter printer = new PrintWriter(writer, true);
			printer.write(stringBuilder.toString());
			printer.flush();
			printer.close();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
//		System.out.println(stringBuilder.toString());

		return function;
	}

	@Override
	public Tree visit(ParameterList parameterList) {
		return null;
	}

	@Override
	public Tree visit(Return r) {
		expressionRoot = null;
		return_on = true;
		r.expression.accept(this);
		return_on = false;
		return r;
	}

	@Override
	public Tree visit(IntArrayElement intArrayElement) {
		return intArrayElement;
	}

	@Override
	public Tree visit(BooleanLiteral booleanLiteral) {
		return booleanLiteral;
	}

	@Override
	public Tree visit(This t) {
		return t;
	}

	@Override
	public Tree visit(NewInitialize newInitialize) {
		return newInitialize;
	}

	@Override
	public Tree visit(NewIntArray newIntArray) {
		return newIntArray;
	}

	@Override
	public Tree visit(ArrayLookup arrayLookup) {
		return arrayLookup;
	}

	@Override
	public Tree visit(LengthOf lengthOf) {
		return lengthOf;
	}

	@Override
	public Tree visit(Dot dot) {
		return dot;
	}

	@Override
	public Tree visit(Program program) {
		for (Class c : program.classes) {
			c.accept(this);
		}
		return program;
	}

	@Override
	public Tree visit(Sidef sidef) {
		return null;
	}

	@Override
	public Tree visit(Yes yes) {
		return null;
	}

	public String DFSDOT(CFGNode node) {
		String result = "";
		for (CFGEdge edge : node.connections) {
			result += node.name;
			result += " -> ";
			result += edge.end.name;
			result += "[label = \"" + edge.s + "\"]";
		}
		return result;
	}


	public Program optimize(Program program){
		for (Class c : program.classes) {
			for (Function f : c.functions) {
				available_analysis(f);
			}
		}
		return program;
	}

	private void available_analysis(Function function) {

	}
}



