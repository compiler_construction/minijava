package MiniJava.Optimization;


import MiniJava.AST.Statement;
import MiniJava.AST.Var;

import java.util.ArrayList;
import java.util.List;

class CFGEdge {
	public CFGNode start;
	public CFGNode end;
	public Statement s;

	public CFGEdge(CFGNode start, CFGNode end, Statement s) {
		this.start = start;
		this.end = end;
		this.s = s;
	}
}

class CFGNode {
	public String name;
	public List<CFGEdge> connections;
	public CFGNode() {
		connections = new ArrayList<>();
	}
}

public class CFGGraph {
	public List<CFGNode> nodes;
	public CFGGraph() {
		nodes = new ArrayList<>();
	}
}
