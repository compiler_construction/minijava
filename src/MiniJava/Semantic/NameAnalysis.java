package MiniJava.Semantic;

import MiniJava.AST.*;
import MiniJava.AST.Class;
import javafx.util.Pair;

import java.util.*;

public class NameAnalysis implements Visitor<Node> {
    boolean errorfree = true;
    Stack<Pair<String, String>> undoStack = new Stack<Pair<String, String>>();
    //This is for the current environment
    Map<String, String> symbolTable = new HashMap<String, String>();
    Stack<Integer> scopeMarkers = new Stack<Integer>();
    Set<String> classSet = new HashSet<>();
    Map<String, Class> classMap = new HashMap<>();
    //This is for field overriding check
    Map<String, List<Pair<String, String>>> classMemberVariable = new HashMap<>();
    //This is for function overriding
    Map<String, List<Function>> classMemberfunction = new HashMap<>();

    boolean isStatic = false;

    boolean isClassVisited = false;

    //for return
    Stack<Pair<String, String>> functionStack = new Stack<>();
    //for this
    Stack<String> classStack = new Stack<>();

    public NameAnalysis() {
        classSet.add("INT");
        classSet.add("BOOLEAN");
        classSet.add("STRING");
        classSet.add("INT-ARRAY");
    }

    public void enterScope() {
        scopeMarkers.push(undoStack.size());
        return;
    }

    public void exitScope() {
        int marker = scopeMarkers.pop();
        while (undoStack.size() > marker) {
            undoStack.pop();
        }
        Stack<Pair<String, String>> copyStack = new Stack<>();
        copyStack = (Stack<Pair<String, String>>) undoStack.clone();
        //clear the table first
        symbolTable.clear();
        //contruct the table
        while (copyStack.size() > 0) {
            Pair<String, String> pair = copyStack.pop();
            symbolTable.put(pair.getKey(), pair.getValue());
        }
    }

    public boolean checkIfDefined(String id) {
        for (Map.Entry<String, String> key : symbolTable.entrySet()) {
            if (key.getKey().equals(id))
                return true;
        }
        return false;
    }

    public String getType(String id) {
        String type = "";
        for (Map.Entry<String, String> key : symbolTable.entrySet()) {
            if (key.getKey().equals(id)) {
                type = key.getValue();
                break;
            }
        }
        return type;
    }

    public boolean checkIfTypeSafe(String ltype, String rtype) {
        if (ltype.equals(rtype)) {
            return true;
        } else {
            return !isSubtype(ltype, rtype);
        }
    }

    public Node visit(Var n) {
        for (List<Function> function : classMemberfunction.values()) {
            if (function.contains(n.varID)) {
                //Since its already defined, do not check
            } else {
                Node node = new Node();
                if (!checkIfDefined(n.varID)) {
                    node.res &= false;
                    errorfree &= node.res;
                    System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + n.varID + "' not defined");
                } else {
                    node.value = n.varID;
                    if (classSet.contains(n.varID)) {
                        node.type = n.varID;
                    } else {
                        node.type = getType(node.value);
                    }
                }
                errorfree &= node.res;
                return node;
            }
        }
        return null;
    }

    public Node visit(IntLiteral n) {
        Node node = new Node();
        node.value = "" + n.value;
        node.type = "INT";
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(StringLiteral n) {
        Node node = new Node();
        node.value = "" + n.value;
        node.type = "STRING";
        errorfree &= node.res;
        return node;
    }

    public Node visit(Plus n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        if (lnode.type.equals("STRING") && rnode.type.equals("INT")) {
            //valid
            node.type = lnode.type;
            node.value = "" + lnode.value + rnode.value;
        } else if (rnode.type.equals("STRING") && lnode.type.equals("INT")) {
            //valid
            node.type = rnode.type;
            node.value = "" + lnode.value + rnode.value;
        } else {
            boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
            if (!typeres) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  :  operands of + operator do not match");
            } else {
                node.type = lnode.type;
                if (!lnode.type.equals("STRING") && !lnode.type.equals("INT") &&
                        !rnode.type.equals("STRING") && !lnode.type.equals("INT")) {
                    node.res &= false;
                    System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for plus operator");
                }
            }
            node.value = "" + lnode.value + " + " + rnode.value;
            node.res &= typeres;
            errorfree &= node.res;
        }
        errorfree &= node.res;
        return node;
    }

    public Node visit(Minus n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  :  operands of - operator do not match");
        } else {
            node.type = lnode.type;
            if (!lnode.type.equals("INT") && !lnode.type.equals("INT")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for minus operator");
            }
        }
        node.value = "" + lnode.value + " - " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;

        return node;
    }

    public Node visit(Times n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  :  operands of * operator do not match");
        } else {
            node.type = lnode.type;
            if (!lnode.type.equals("INT") && !lnode.type.equals("INT")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for times operator");
            }
        }
        node.value = "" + lnode.value + " * " + rnode.value;
        node.res &= node.res && typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Division n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  :  operands of / operator do not match");
        } else {
            node.type = lnode.type;
            if (!lnode.type.equals("INT") && !lnode.type.equals("INT")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for division operator");
            }
        }
        node.value = "" + lnode.value + " / " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Modulo n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + " and " + rnode.value + "' not of same type");
        } else {
            node.type = lnode.type;
            if (!lnode.type.equals("INT") && !lnode.type.equals("INT")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for modulo operator");
            }
        }
        node.value = "" + lnode.value + " % " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Equals n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            if (!(classMap.containsKey(lnode.type) && classMap.containsKey(rnode.type))) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + " and " + rnode.value + "' not of same type");
            }
        } else {
            node.type = "BOOLEAN";
        }
        node.value = "" + lnode.value + " == " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(GreaterThan n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + " and " + rnode.value + "' not of same type");
        } else {
            if (!lnode.type.equals("INT") && !lnode.type.equals("INT")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for comparision operator");
            } else {
                node.type = "BOOLEAN";
            }
        }
        node.value = "" + lnode.value + " > " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(LessThan n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + " and " + rnode.value + "' not of same type");
        } else {
            node.type = lnode.type;
            if (!lnode.type.equals("INT") && !lnode.type.equals("INT")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for comparision operator");
            } else {
                node.type = "BOOLEAN";
            }
        }
        node.value = "" + lnode.value + " < " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(And n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + " and " + rnode.value + "' not of same type");
        } else {
            if (!lnode.type.equals("BOOLEAN") && !lnode.type.equals("BOOLEAN")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for comparision operator");
            } else {
                node.type = "BOOLEAN";
            }
        }
        node.value = "" + lnode.value + " && " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Or n) {
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.lhs.accept(this);
        rnode = n.rhs.accept(this);

        Node node = new Node();
        node.res &= lnode.res && rnode.res;

        boolean typeres = checkIfTypeSafe(lnode.type, rnode.type);
        if (!typeres) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + " and " + rnode.value + "' not of same type");
        } else {
            if (!lnode.type.equals("BOOLEAN") && !lnode.type.equals("BOOLEAN")) {
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + lnode.value + "' and '" + rnode.value + "' not valid operands for comparision operator");
            } else {
                node.type = "BOOLEAN";
            }
        }
        node.value = "" + lnode.value + " || " + rnode.value;
        node.res &= typeres;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Neg n) {
        Node node = new Node();
        node = n.accept(this);
        if (!node.type.equals("BOOLEAN")) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + node.value + "' not valid operand for negation operator");
        } else {
            node.type = "BOOLEAN";
        }
        errorfree &= node.res;
        return node;
    }

    public Node visit(Not n) {
        Node node = new Node();
        node = n.expr.accept(this);
        if (!node.type.equals("BOOLEAN")) {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + node.value + "' not valid operand for not operator");
        } else {
            node.type = "BOOLEAN";
        }
        errorfree &= node.res;
        return node;
    }

//	public String visit( Identifier)

    // ##############   Statements   ##############

    public Node visit(IfThenElse n) {

        Node node = new Node();
        Node cnode = n.expr.accept(this);
        if (!cnode.type.equals("BOOLEAN")) {
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : condition '" + cnode.value + "' is not of type BOOLEAN");
            //invalid
            node.res &= false;
        }
        Node snode = new Node();
        snode = n.then.accept(this);
        Node enode = new Node();
        enode = n.elze.accept(this);
        if (!snode.type.equals("VOID")) {
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + snode.value + "' statements should have void as a return type");
            node.res &= snode.res;
            errorfree &= node.res;
            return node;
        }
        if (!enode.type.equals("VOID")) {
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + enode.value + "' statements should have void as a return type");
            node.res &= enode.res;
            errorfree &= node.res;
            return node;
        }
        errorfree &= node.res;
        return node;
    }

    public Node visit(Print n) {
        Node node = new Node();
        node = n.e.accept(this);
        if (node.type.equals("INT") ||
                node.type.equals("STRING") ||
                node.type.equals("BOOLEAN")) {

        } else {
            node.res &= false;
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : argument to println is of invalid type");
        }
        errorfree &= node.res;
        return node;
    }

    public Node visit(Assign n) {
        Node node = new Node();
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.varID.accept(this);
        rnode = n.expr.accept(this);
        if (!(lnode.res && rnode.res)) {
            node.res &= false;
        } else {
//            node.res = true;
        }
        if (!checkIfTypeSafe(lnode.type, rnode.type)) {
            if (!(lnode.type.equals("INT-ARRAY") && rnode.type.equals("INT"))) {
                if (classMap.containsKey(lnode.type) && classMap.containsKey((rnode.type))) {
                    //for subtype check
                    if (!isSubtype(rnode.type, lnode.type)) {
                        node.res &= false;
                        System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  :  operands of = operator do not match");
                    } else {
                        //if a subtype; the LHS will have a new type which is a subtype of original type
                        //this.symbolTable.put(lnode.value,rnode.type);
                    }
                    errorfree &= node.res;
                    return node;
                }
                node.res &= false;
                System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  :  operands of = operator do not match");
            }
        }
        errorfree &= node.res;
        return node;
    }

    public Node visit(While n) {
        Node node = new Node();
        Node cnode = n.expr.accept(this);
        if (!cnode.type.equals("BOOLEAN")) {
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : condition '" + cnode.value + "' is not of type BOOLEAN");
            //invalid
            node.res &= false;
        }
        Node snode = new Node();
        snode = n.body.accept(this);
        if (!snode.type.equals("VOID")) {
            System.err.println(n.getLine() + 1 + ":" + (n.getCol() + 1) + " error  : '" + snode.value + "' statements should have void as a return type");
            node.res &= snode.res;
        }
        errorfree &= node.res;
        return node;
    }

    public Node visit(For n) {
        Node node = new Node();
        node.res &= n.init.accept(this).res && n.expr.accept(this).res && n.step.accept(this).res && n.body.accept(this).res;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Block n) {
        Node node = new Node();
        Boolean b = true;
        for (Statement s : n.body) {
            b &= s.accept(this).res;
        }
        node.res &= b;
        errorfree &= node.res;
        return node;
    }

    public Node visit(Skip n) {
        Node node = new Node();
        errorfree &= node.res;
        return node;
    }

    //for type
    public Node visit(Type t) {
        Node node = new Node();
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(Class v) {
        boolean b = true;
        Node node = new Node();
        if (v.parentClass != "" && !classSet.contains(v.parentClass)) {
            System.err.println(v.getLine() + 1 + ":" + (v.getCol() + 1) + " error: '" + v.parentClass + "' is not defined");
            b = false;
            errorfree &= b;
        }
        //this is for checking circular graph inheritance
        Map<String, Boolean> visitedClass = new HashMap<>();
        for (String s : classSet) {
            visitedClass.put(s, false);
        }
        if (isCircular(v, visitedClass)) {
            b = false;
            System.err.println(v.getLine() + 1 + ":" + (v.getCol() + 1) + " error: '" +
                    v.id.varID + "' inheritance graph has a cycle");
        }
        enterScope();
        //copy the all the variables from the parent class or its grand parents
        for (String s : classSet) {
            visitedClass.put(s, false);
        }
        copyParentFieldsIntoSymbolTable(v, visitedClass);
        for (VarDecl varDecl : v.varDecls) {
            //This is for adding variables of corresponding class
            if (symbolTable.containsKey(varDecl.var.varID)) {
                System.err.println(varDecl.getLine() + 1 + ":" + (v.getCol() + 1) + " error: '" +
                        varDecl.var.varID + "' is already defined");
                b = false;
                errorfree &= b;
            } else {
                if (classMemberVariable.containsKey(v.id.varID)) {
                    classMemberVariable.get(v.id.varID).add(new Pair(varDecl.var.varID, varDecl.type.type));
                }
                // what if the current variable is defined in the parent (grand pa) class
                for (String s : visitedClass.keySet()) {
                    visitedClass.put(s, false);
                }
                boolean overriding = checkIfVariableOverriding(varDecl.var.varID, v, visitedClass);
                if (overriding) {
                    b = false;
                    System.err.println(varDecl.var.getLine() + 1 + ":" + (varDecl.var.getCol() + 1) + " error: '" + varDecl.var.varID + "' is overriden in its parent class");
                }
            }
            b = varDecl.accept(this).res;
            node.res &= b;
        }
        for (Function function : v.functions) {
            if (symbolTable.containsKey(function.id.varID)) {
                System.err.println(function.getLine() + 1 + ":" + (function.getCol() + 1) + " error: function '" + function.id.varID + "' is already declared");
                errorfree &= false;
            }
            if (classMemberfunction.containsKey(v.id.varID)) {
                classMemberfunction.get(v.id.varID).add(function);
            }
            for (String s : classSet) {
                visitedClass.put(s, false);
            }
            if (checkIfFunctionOverridingWithDifferentArgs(function, v, visitedClass)) {
                b = false;
                System.err.println(function.getLine() + 1 + ":" + (function.getCol() + 1) + " error: '" + function.id.varID + "' is overriden with different parameter types");
            }
            undoStack.push(new Pair<String, String>(function.id.varID, function.returnType.type));
            symbolTable.put(function.id.varID, function.returnType.type);
        }
        for (Function function : v.functions) {
            functionStack.push(new Pair<String, String>(function.id.varID, function.returnType.type));
            //for double pass through all the functions of all the classes
            if (isClassVisited)
                b = function.accept(this).res;
            node.res &= b;
        }
        if(!classStack.empty())
            classStack.pop();
        exitScope();
        errorfree &= node.res;
        return node;
    }

    private void copyParentFieldsIntoSymbolTable(Class v, Map<String, Boolean> visitedClass) {
        if (visitedClass.containsKey(v.id.varID) && visitedClass.get(v.id.varID)) {
            return;
        }
        visitedClass.put(v.id.varID, true);
        if (classMemberVariable.containsKey(v.parentClass)) {
            for (Pair<String, String> p : classMemberVariable.get(v.parentClass)) {
                undoStack.push(p);
                symbolTable.put(p.getKey(), p.getValue());
            }
            copyParentFieldsIntoSymbolTable(classMap.get(v.parentClass), visitedClass);
        }
    }

    private Boolean checkIfVariableOverriding(String varID, Class clazz, Map<String, Boolean> visitedClass) {
        if (visitedClass.containsKey(clazz.id.varID) && visitedClass.get(clazz.id.varID)) {
            // it is a circular inheritance my friend
            System.err.println("It does not make any sense to check overriding variables inside a circular inheritance graph");
            return true;
        } else {
            visitedClass.put(clazz.id.varID, true);
            // check if a variable is defined in the parent class
            if (classMemberVariable.containsKey(clazz.parentClass)) {
                String parent = clazz.parentClass;
                for (Pair<String, String> pair : classMemberVariable.get(parent)) {
                    String id = pair.getKey();
                    if (id.equals(varID)) {
                        return true;
                    }
                }
                if (classMap.containsKey(clazz.parentClass)) {
                    return checkIfVariableOverriding(varID, classMap.get(clazz.parentClass), visitedClass);
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    private Boolean checkIfFunctionOverridingWithDifferentArgs(Function function, Class clazz, Map<String, Boolean> visitedClass) {
        if (visitedClass.containsKey(clazz.id.varID) && visitedClass.get(clazz.id.varID)) {
            // it is a circular inheritance my friend
            System.err.println("It does not make any sense to check overriding functions inside a circular inheritance graph");
            return true;
        } else {
            visitedClass.put(clazz.id.varID, true);
            // check if a function is having different arguments
            if (classMemberfunction.containsKey(clazz.parentClass)) {
                String parent = clazz.parentClass;
                for (Function f : classMemberfunction.get(parent)) {
                    String id = f.id.varID;
                    if (id.equals(function.id.varID)) {
                        Boolean b = true;
                        if (!function.returnType.type.equals("INT") && !function.returnType.type.equals("INT-ARRAY") &&
                                !function.returnType.type.equals("STRING") && !function.returnType.type.equals("BOOLEAN") &&
                                !f.returnType.type.equals("INT") && !f.returnType.type.equals("INT-ARRAY") &&
                                !f.returnType.type.equals("STRING") && !f.returnType.type.equals("BOOLEAN")) {
                            if (!isSubtype(function.returnType.type, f.returnType.type)) {
                                b &= true;
                                System.err.println(function.getLine() + 1 + ":" + (function.getCol() + 1) + " error: return type of: '" + clazz.id.varID + " :: " + function.id.varID + "' and '" + parent + " :: " + f.id.varID + "' do not match");
                            }
                        }
                        if (f.parameterList.parameters.size() == function.parameterList.parameters.size()) {
                            for (int i = 0; i < f.parameterList.parameters.size(); i++) {
                                VarDecl varDecl = f.parameterList.parameters.get(i);
                                if (!varDecl.type.type.equals(function.parameterList.parameters.get(i).type.type)) {
                                    if (isSubtype(function.parameterList.parameters.get(i).type.type, varDecl.type.type)) {
                                        return false;
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                        return true;
                    }
                }
                if (classMap.containsKey(clazz.parentClass)) {
                    return checkIfVariableOverriding(function.id.varID, classMap.get(clazz.parentClass), visitedClass);
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    public boolean isCircular(Class clazz, Map<String, Boolean> visitedClass) {
        boolean visited = visitedClass.get(clazz.id.varID);
        String parentName = clazz.parentClass;
        if (visited) {
            return true;
        } else {
            visitedClass.put(clazz.id.varID, true);
            if (classMap.containsKey(parentName)) {
                return isCircular(classMap.get(parentName), visitedClass);
            } else {
                return false;
            }
        }
    }

    public boolean isSubtype(String subclass, String superclass) {
        boolean b = false;
        if ((classMap.containsKey(subclass) && classMap.containsKey(superclass))) {
            while (!classMap.get(subclass).parentClass.equals("")) {
                if (classMap.get(subclass).parentClass.equals(superclass)) {
                    b = true;
                    return b;
                }
                subclass = classMap.get(subclass).parentClass;
            }
        }
        return b;
    }

    @Override
    public Node visit(VarDecl varDecl) {
        Node node = new Node();
        Boolean b = true;
        if (!classSet.contains(varDecl.type.type)) {
            System.err.println(varDecl.type.getLine() + 1 + ":" + (varDecl.type.getCol() + 1) + " error: type '" + varDecl.type.type + "' is not defined");
            b = false;
            errorfree &= b;
        }
        undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
        symbolTable.put(varDecl.var.varID, varDecl.type.type);
        node.res &= b;
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(Function function) {
        isStatic = function.isStatic;
        Node node = new Node();
        Boolean b = true;
        //Shadowing is allowed after this
        enterScope();
        //Symbol table in the function scope, you cannot shadow local variables inside function scope
        Map<String, String> VarSymbolTable = new HashMap<>();
        for (VarDecl varDecl : function.parameterList.parameters) {
            if (VarSymbolTable.containsKey(varDecl.var.varID)) {
                System.err.println(varDecl.getLine() + 1 + ":" + (varDecl.getCol() + 1) + " error: '" +
                        varDecl.var.varID + "' is already defined");
                b = false;
                errorfree &= b;
            }
            VarSymbolTable.put(varDecl.var.varID, varDecl.type.type);
            symbolTable.put(varDecl.var.varID, varDecl.type.type);
            undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
        }

        for (Statement statement : function.block.body) {
            if (statement instanceof VarDecl) {
                VarDecl varDecl = (VarDecl) statement;
                if (VarSymbolTable.containsKey(varDecl.var.varID)) {
                    System.err.println(varDecl.getLine() + 1 + ":" + (varDecl.getCol() + 1) + " error: '" +
                            varDecl.var.varID + "' is already defined");
                    b = false;
                    errorfree &= b;
                }
                VarSymbolTable.put(varDecl.var.varID, varDecl.type.type);
                symbolTable.put(varDecl.var.varID, varDecl.type.type);
                undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
            }
            b = statement.accept(this).res;
        }
        if (!functionStack.empty())
            functionStack.pop();
        exitScope();
        node.res &= b;
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(ParameterList parameterList) {
        Node node = new Node();
        Boolean b = true;
        Map<String, String> parameterSymbolTable = new HashMap<>();
        for (VarDecl varDecl : parameterList.parameters) {
            if (parameterSymbolTable.containsKey(varDecl.var.varID)) {
                System.err.println(varDecl.getLine() + 1 + ":" + (varDecl.getCol() + 1) + " error: '" +
                        varDecl.var.varID + "' is already defined");
                b = false;
                errorfree &= b;
            } else {
                parameterSymbolTable.put(varDecl.var.varID, varDecl.type.type);
                undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
            }
        }
        symbolTable.putAll(parameterSymbolTable);
        node.res &= b;
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(Return r) {
        Node node = new Node();
        node = r.expression.accept(this);
//        if (isClassVisited) {
        Pair<String, String> functionReturn = functionStack.peek();
        if (!node.type.equals(functionReturn.getValue())) {
            if (!isSubtype(node.type, functionReturn.getValue())) {
                node.res &= false;
                System.err.println(r.getLine() + 1 + ":" + (r.getCol() + 1) + " error: type of '" +
                        node.value + "': " + node.type + " does not match with return type of  function '" + functionReturn.getKey() + "' which is: " +
                        functionReturn.getValue());
            }
        }
//        }
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(IntArrayElement intArrayElement) {
        Node node = new Node();
        node.res &= checkIfDefined(intArrayElement.varID) && intArrayElement.expression.accept(this).res;
        node.type = getType(intArrayElement.varID);
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(BooleanLiteral booleanLiteral) {
        Node node = new Node();
        node.value = "" + booleanLiteral.value;
        node.type = "BOOLEAN";
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(This t) {
        Node node = new Node();
        if (isStatic) {
            System.err.println(t.getLine() + 1 + ":" + (t.getCol() + 1) + " error: 'this' is disallowed in a static method");
        } else {
//            if (isClassVisited) {
            String Class = classStack.peek();
            node.value = Class;
            node.type = Class;
        }
//        }
        node.res = !isStatic;
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(NewInitialize newInitialize) {
        Node node = new Node();
        node = newInitialize.id.accept(this);
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(NewIntArray newIntArray) {
        Node node = new Node();
        node = newIntArray.expression.accept(this);
        if (!node.type.equals("INT")) {
            node.res &= false;
            System.err.println(newIntArray.getLine() + 1 + ":" + (newIntArray.getCol() + 1) + " error: '" + node.value + "' not of type 'INT'");
        }
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(ArrayLookup arrayLookup) {
        Node node = new Node();
        node.res &= arrayLookup.id.accept(this).res && arrayLookup.lookup.accept(this).res;
        node.type = "INT";
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(LengthOf lengthOf) {
        Node node = new Node();
        node = lengthOf.var.accept(this);
        if (!node.type.equals("INT-ARRAY")) {
            node.res &= false;
            System.err.println(lengthOf.getLine() + 1 + ":" + (lengthOf.getCol() + 1) + " error: '" + node.value + "' not of type 'INT-ARRAY'");
        }
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(Dot dot) {
        Node node = new Node();
        Boolean b = true;
        Node cnode = dot.expression.accept(this);

        if (!classSet.contains(cnode.type)) {
            b &= false;
            System.err.println(dot.getLine() + 1 + ":" + (dot.getCol() + 1) + " error: invalid use of new as the class does not exist");
        } else {
            List<Function> fl = classMemberfunction.get(cnode.type);
            ListIterator<Function> itr = fl.listIterator();
            while (itr.hasNext()) {
                Function f = itr.next();
                if (f.id.varID.equals(dot.id.varID)) {
                    Function fn = f;
                    symbolTable.put(fn.id.varID, fn.returnType.type);
                    if (f.parameterList.parameters.size() == dot.expressions.size()) {
                        if (f.parameterList.parameters.size() == 0) {
                            undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                            if (!functionStack.peek().getKey().equals(fn.id.varID) )
                                functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                            symbolTable.put(fn.id.varID, fn.returnType.type);
                        }
                        for (int i = 0; i < f.parameterList.parameters.size(); i++) {
                            if (checkIfTypeSafe(f.parameterList.parameters.get(i).type.type, dot.expressions.get(i).accept(this).type)) {
                                undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                symbolTable.put(fn.id.varID, fn.returnType.type);
                                if (!functionStack.peek().getKey().equals(fn.id.varID) )
                                    functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                            } else {
                                //print error
                                System.err.println(dot.getLine() + 1 + ":" + (dot.getCol() + 1) + " error: '" + dot.id.varID + "' is passed with invalid argument type");
                                b &= false;
                                errorfree &= b;
                            }
                        }

                    } else {
                        //print error
                        System.err.println(dot.getLine() + 1 + ":" + (dot.getCol() + 1) + " error: '" + dot.id.varID + "' is passed with invalid argument type");
                        b &= false;
                        errorfree &= b;
                    }
                    break;
                }
            }
            while (classMap.get(cnode.type).parentClass != "" && !symbolTable.containsKey(dot.id.varID)) {
                fl = classMemberfunction.get(classMap.get(cnode.type).parentClass);
                itr = fl.listIterator();
                while (itr.hasNext()) {
                    Function f = itr.next();
                    if (f.id.varID.equals(dot.id.varID)) {
                        Function fn = f;
                        symbolTable.put(fn.id.varID, fn.returnType.type);
                        if (f.parameterList.parameters.size() == dot.expressions.size()) {
                            if (f.parameterList.parameters.size() == 0) {
                                undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                if (!functionStack.peek().getKey().equals(fn.id.varID) )
                                    functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                symbolTable.put(fn.id.varID, fn.returnType.type);
                            }
                            for (int i = 0; i < f.parameterList.parameters.size(); i++) {
                                if (checkIfTypeSafe(f.parameterList.parameters.get(i).type.type, dot.expressions.get(i).accept(this).type)) {
                                    undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                    if (!functionStack.peek().getKey().equals(fn.id.varID) )
                                        functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                    symbolTable.put(fn.id.varID, fn.returnType.type);
                                } else {
                                    //print error
                                    System.err.println(dot.getLine() + 1 + ":" + (dot.getCol() + 1) + " error: '" + dot.id.varID + "' is passed with invalid argument type");
                                    b &= false;
                                    errorfree &= b;
                                }
                            }

                        } else {
                            //print error
                            System.err.println(dot.getLine() + 1 + ":" + (dot.getCol() + 1) + " error: '" + dot.id.varID + "' is passed with invalid argument type");
                            b &= false;
                            errorfree &= b;
                        }
                        break;
                    }
                }
                cnode.type = classMap.get(cnode.type).parentClass;
            }
            if (symbolTable.containsKey(dot.id.varID)) {
                functionStack.pop();
            }
            Node fnode = dot.id.accept(this);
            Node pnode = new Node();
            b &= cnode.res;
            b &= fnode.res;
            for (Expression e : dot.expressions) {
                pnode = e.accept(this);
                b &= pnode.res;
            }
            node.type = fnode.type;
            node.res &= b;
            node.value = fnode.value;
        }
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(Program program) {
        Boolean b = true;
        undoStack.push(new Pair<>(program.mainClass.id.varID, "Class"));
        symbolTable.put(program.mainClass.id.varID, "Class");
        classSet.add(program.mainClass.id.varID);
        classMap.put(program.mainClass.id.varID, program.mainClass);
        for (Class c : program.classes) {
            if (symbolTable.containsKey(c.id.varID)) {
                System.err.println(c.getLine() + 1 + ":" + (c.getCol() + 1) + " error: '" + c.id.varID + "' is already defined");
                b &= false;
                errorfree &= b;
            } else {
                undoStack.push(new Pair<>(c.id.varID, "Class"));
                symbolTable.put(c.id.varID, "Class");
                classSet.add(c.id.varID);
                classMap.put(c.id.varID, c);
                classMemberVariable.put(c.id.varID, new ArrayList<>());
                classMemberfunction.put(c.id.varID, new ArrayList<>());
            }
        }
        for (Class c : program.classes) {
            classStack.push(c.id.varID);
            b &= c.accept(this).res;
        }
        isClassVisited = true;
        for (Class c : program.classes) {
            classStack.push(c.id.varID);
            b &= c.accept(this).res;
        }
        program.mainClass.accept(this);

        Node node = new Node();
        node.res &= b;
        node.res = errorfree;
        errorfree &= node.res;
        return node;
    }

    @Override
    public Node visit(Sidef sidef) {
        return sidef.expression.accept(this);
    }

    @Override
    public Node visit(Yes yes) {
        return null;
    }

    //Bang
}