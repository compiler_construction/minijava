package MiniJava.Semantic;

public class NameCollisionException extends RuntimeException{

    public NameCollisionException(String msg){
        super(msg);
    }
}