package MiniJava;
import MiniJava.AST.Program;
import MiniJava.AST.TreePrinter;
import MiniJava.CodeGen.CodeGenerator;
import MiniJava.Lexer.Lexer;
import MiniJava.Lexer.Token;
import MiniJava.Lexer.Type;
import MiniJava.Optimization.CFGGenerator;
import MiniJava.Semantic.NameAnalysis;
import MiniJava.Syntax.SyntaxParseError;
import MiniJava.Syntax.SyntaxParser;


import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * This class is driver for lexing the source file.
 * Author: Xu Ruiyang, Akhil Pawar
 */

public class emjc {
	protected List<Token> tokens;	//List of lexed tokens

	public emjc() {
		tokens = new ArrayList<>();
	}
	/**
	 * Runs the scanner on input files.
	 *
	 * This is a standalone scanner, it will print any unmatched
	 * text to System.out unchanged.
	 *
	 * @param argv   the command line, contains the filenames to run
	 *               the scanner on.
	 */
	public static void main(String argv[]) {
		if (argv.length == 0 || argv.length ==1) {
			System.out.println("Usage : java Minijava.emjc --lex <inputfile>");
		}
		else {
			emjc compiler = new emjc();
			int firstFilePos = 0;
			String encodingName = "UTF-8";
			if (argv[0].equals("--lex")) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
					Lexer scanner = null;		// To read
					String output = "";			// Output string
					Writer fw = null;
					try {
						java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
						java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
						scanner = new Lexer(reader);
						while (true) {
							Type current = scanner.yylex();
							Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
							compiler.tokens.add(current_token);			// Store token
							output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
									" " + current_token.toString() +"\n";
							if (current == Type.EOF || current == null) {
								break;
							}
						}
						// Write to file
						fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(argv[1] + ".lexed")));
						fw.write(output);
					}
					catch (java.io.FileNotFoundException e) {
						System.out.println("File not found : \""+argv[i]+"\"");
					}
					catch (java.io.IOException e) {
						System.out.println("IO error scanning file \""+argv[i]+"\"");
						System.out.println(e);
					}
					catch (Exception e) {
						System.out.println("Unexpected exception:");
						e.printStackTrace();
					}
					finally {
						try{
							fw.close();
						}
						catch (Exception ex){
						}
					}
				}
			} else if (argv[0].equals("--ast")) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
					Lexer scanner = null;		// To read
					String output = "";			// Output string
					Writer fw = null;			// To write
					try {
						java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
						java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
						scanner = new Lexer(reader);
						while (true) {
							Type current = scanner.yylex();
							Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
							compiler.tokens.add(current_token);			// Store token
							output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
									" " + current_token.toString() +"\n";
							if (current == Type.EOF || current == null) {
								break;
							}
						}
						TreePrinter treePrinter = new TreePrinter();
						SyntaxParser syntaxParser = new SyntaxParser(compiler.tokens);
						NameAnalysis nameChecker = new NameAnalysis();
						Program program = syntaxParser.parsing();
						if (argv[0].equals("--ast")) {
							output = program.accept(treePrinter);
							// Write to file
							fw = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(argv[1] + ".ast")));
							fw.write(output);
						}
					}
					catch (java.io.FileNotFoundException e) {
						System.out.println("File not found : \""+argv[i]+"\"");
					}
					catch (java.io.IOException e) {
						System.out.println("IO error scanning file \""+argv[i]+"\"");
						System.out.println(e);
					}
					catch (SyntaxParseError e) {
						System.err.println(e.getMessage());
						e.printStackTrace();
					}
					finally {
						try{
							fw.close();
						}
						catch (Exception ex){

						}
					}
				}
			}  else if (argv[0].equals("--name") || argv[0].equals("--type")) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
                    Lexer scanner = null;		// To read
                    String output = "";			// Output string
                    Writer fw = null;			// To write
                    try {
                        java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
                        java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
                        scanner = new Lexer(reader);
                        while (true) {
                            Type current = scanner.yylex();
                            Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
                            compiler.tokens.add(current_token);			// Store token
                            output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
                                    " " + current_token.toString() +"\n";
                            if (current == Type.EOF || current == null) {
                                break;
                            }
                        }
                        SyntaxParser syntaxParser = new SyntaxParser(compiler.tokens);
                        NameAnalysis nameChecker = new NameAnalysis();
                        Program program = syntaxParser.parsing();
						Boolean b = program.accept(nameChecker).res;
						if (b) {
							System.out.println("Valid eMiniJava Program");
						}
                    }
                    catch (java.io.FileNotFoundException e) {
                        System.out.println("File not found : \""+argv[i]+"\"");
                    }
                    catch (java.io.IOException e) {
                        System.out.println("IO error scanning file \""+argv[i]+"\"");
                        System.out.println(e);
                    }
                    catch (SyntaxParseError e) {
                        System.err.println(e.getMessage());
                        e.printStackTrace();
                    }
                    finally {
                        try{
                            fw.close();
                        }
                        catch (Exception ex){

                        }
                    }
                }
            } else if (argv[0].equals("--cgen") ) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
					Lexer scanner = null;		// To read
					String output = "";			// Output string
					Writer fw = null;			// To write
					try {
						java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
						java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
						scanner = new Lexer(reader);
						while (true) {
							Type current = scanner.yylex();
							Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
							compiler.tokens.add(current_token);			// Store token
							output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
									" " + current_token.toString() +"\n";
							if (current == Type.EOF || current == null) {
								break;
							}
						}
						SyntaxParser syntaxParser = new SyntaxParser(compiler.tokens);
						NameAnalysis nameChecker = new NameAnalysis();
						Program program = syntaxParser.parsing();
						Boolean b = program.accept(nameChecker).res;
						if (b) {
							System.out.println("Valid eMiniJava Program");
							CodeGenerator codegenerator = new CodeGenerator();
							program.accept(codegenerator);
						}
					}
					catch (java.io.FileNotFoundException e) {
						System.out.println("File not found : \""+argv[i]+"\"");
					}
					catch (java.io.IOException e) {
						System.out.println("IO error scanning file \""+argv[i]+"\"");
						System.out.println(e);
					}
					catch (SyntaxParseError e) {
						System.err.println(e.getMessage());
						e.printStackTrace();
					}
					finally {
						try{
							fw.close();
						}
						catch (Exception ex){

						}
					}
				}
			} else if (argv[0].equals("--cfg") ) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
					Lexer scanner = null;		// To read
					String output = "";			// Output string
					Writer fw = null;			// To write
					try {
						java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
						java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
						scanner = new Lexer(reader);
						while (true) {
							Type current = scanner.yylex();
							Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
							compiler.tokens.add(current_token);			// Store token
							output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
									" " + current_token.toString() +"\n";
							if (current == Type.EOF || current == null) {
								break;
							}
						}
						SyntaxParser syntaxParser = new SyntaxParser(compiler.tokens);
						Program program = syntaxParser.parsing();
						CFGGenerator generator = new CFGGenerator();
						generator.visit(program);
					}
					catch (java.io.FileNotFoundException e) {
						System.out.println("File not found : \""+argv[i]+"\"");
					}
					catch (java.io.IOException e) {
						System.out.println("IO error scanning file \""+argv[i]+"\"");
						System.out.println(e);
					}
					catch (SyntaxParseError e) {
						System.err.println(e.getMessage());
						e.printStackTrace();
					}
					finally {
						try{
							fw.close();
						}
						catch (Exception ex){

						}
					}
				}
			} else if (argv[0].equals("--opt") ) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
					Lexer scanner = null;		// To read
					String output = "";			// Output string
					Writer fw = null;			// To write
					try {
						java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
						java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
						scanner = new Lexer(reader);
						while (true) {
							Type current = scanner.yylex();
							Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
							compiler.tokens.add(current_token);			// Store token
							output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
									" " + current_token.toString() +"\n";
							if (current == Type.EOF || current == null) {
								break;
							}
						}
						SyntaxParser syntaxParser = new SyntaxParser(compiler.tokens);
						Program program = syntaxParser.parsing();
						CFGGenerator generator = new CFGGenerator();
						generator.visit(program);
						program = generator.optimize(program);
						CodeGenerator codegenerator = new CodeGenerator();
						program.accept(codegenerator);
					}
					catch (java.io.FileNotFoundException e) {
						System.out.println("File not found : \""+argv[i]+"\"");
					}
					catch (java.io.IOException e) {
						System.out.println("IO error scanning file \""+argv[i]+"\"");
						System.out.println(e);
					}
					catch (SyntaxParseError e) {
						System.err.println(e.getMessage());
						e.printStackTrace();
					}
					finally {
						try{
							fw.close();
						}
						catch (Exception ex){

						}
					}
				}
			} else if (argv[0].equals("--optinfo") ) {
				firstFilePos = 1;
				for (int i = firstFilePos; i < argv.length; i++) {
					Lexer scanner = null;		// To read
					String output = "";			// Output string
					Writer fw = null;			// To write
					try {
						java.io.FileInputStream stream = new java.io.FileInputStream(argv[i]);
						java.io.Reader reader = new java.io.InputStreamReader(stream, encodingName);
						scanner = new Lexer(reader);
						while (true) {
							Type current = scanner.yylex();
							Token current_token = new Token(scanner.yytext(), current, scanner.get_yy_line(), scanner.get_yy_column());
							compiler.tokens.add(current_token);			// Store token
							output += "" + (scanner.get_yy_line() + 1) + ":" + (scanner.get_yy_column() + 1) +
									" " + current_token.toString() +"\n";
							if (current == Type.EOF || current == null) {
								break;
							}
						}
						SyntaxParser syntaxParser = new SyntaxParser(compiler.tokens);
						Program program = syntaxParser.parsing();
						CodeGenerator codegenerator = new CodeGenerator();
						CFGGenerator generator = new CFGGenerator();
						program.accept(codegenerator);

						generator.visit(program);
						program = generator.optimize(program);
						program.accept(codegenerator);
						System.out.println("#bytecode before optimization:" + codegenerator.num_code_generated);
						System.out.println("#bytecode after optimization:" + generator.graphs.get(0).nodes);
					}
					catch (java.io.FileNotFoundException e) {
						System.out.println("File not found : \""+argv[i]+"\"");
					}
					catch (java.io.IOException e) {
						System.out.println("IO error scanning file \""+argv[i]+"\"");
						System.out.println(e);
					}
					catch (SyntaxParseError e) {
						System.err.println(e.getMessage());
						e.printStackTrace();
					}
					finally {
						try{
							fw.close();
						}
						catch (Exception ex){

						}
					}
				}
			}
		}
	}
}
