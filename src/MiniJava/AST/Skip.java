package MiniJava.AST;

/* Skip represents empty statement. 
 * It is useful e.g. as "else" block of and if-then-else statement
 */
public class Skip extends SingleStatement {
	public Skip(int l, int c) {
		super(l,c);
	};
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
};