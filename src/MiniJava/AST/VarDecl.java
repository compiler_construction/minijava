package MiniJava.AST;

public class VarDecl extends Statement {
	public Type type;
	public Var var;
	public VarDecl(Type type, Var var, int l, int c) {
		super(l,c);
		this.type = type;
		this.var = var;
	}
	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
