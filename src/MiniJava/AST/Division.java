package MiniJava.AST;

public class Division extends Expression {
	public Expression lhs;
	public Expression rhs;
	public Division(Expression lhs,	Expression rhs, int l, int c) {
		super(l,c);
		this.lhs = lhs; this.rhs = rhs;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return lhs + " / " + rhs;
	}
}
