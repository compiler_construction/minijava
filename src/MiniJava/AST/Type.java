package MiniJava.AST;
public class Type extends Tree {
	public String type;
	public Type(String v, int l, int c) {
		super(l,c);
		type = v;
	}
	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
