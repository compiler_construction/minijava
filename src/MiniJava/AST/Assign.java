package MiniJava.AST;

public class Assign extends SingleStatement {
	public Var varID;
	public Expression expr;
	public Assign(Var varID, Expression expr, int l, int c) {
		super(l,c);
		this.varID = varID; this.expr = expr;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	public String toString() {
		return varID + " = " + expr;
	}
}
