package MiniJava.AST;

public class StringLiteral extends Expression {
	public String value;
	public StringLiteral(String value, int l, int c) {
		super(l,c);
		this.value = value;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return value;
	}
}

