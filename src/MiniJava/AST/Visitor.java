package MiniJava.AST;

public interface Visitor<R> {
	// Statement
	public R visit(Print n);
	public R visit(Assign n);
	public R visit(Skip n);
	public R visit(Block n);
	public R visit(IfThenElse n);
	public R visit(While n);
	public R visit(For n);
	
	// Expression
	public R visit(Var n);
	public R visit(IntLiteral n);
	public R visit(StringLiteral n);
	public R visit(Plus n);
	public R visit(Minus n);
	public R visit(Times n);
	public R visit(Division n);
	public R visit(Modulo n);
	public R visit(Equals n);
	public R visit(GreaterThan n);
	public R visit(LessThan n);
	public R visit(And n);
	public R visit(Or n);
	public R visit(Neg n);
	public R visit(Not n);

	//type
	public R visit(Type t);

	//class
	public R visit(Class v);

	//fields
	public R visit(VarDecl varDecl);

	//function
	public R visit(Function function);
	public R visit(ParameterList parameterList);
	public R visit(Return r);

	public R visit(IntArrayElement intArrayElement);

	public R visit(BooleanLiteral booleanLiteral);

	public R visit(This t);

	public R visit(NewInitialize newInitialize);

	public R visit(NewIntArray newIntArray);

	public R visit(ArrayLookup arrayLookup);

	public R visit(LengthOf lengthOf);

	public R visit(Dot dot);

	public R visit(Program program);

	public R visit(Sidef sidef);

	public R visit(Yes yes);

}