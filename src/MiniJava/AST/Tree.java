package MiniJava.AST;
 
public abstract class Tree {
	int line;
	int col;
	public abstract <R> R accept(Visitor<R> v);
	public Tree(int line,int col){
		this.line = line;
		this.col = col;
	}
	public int getLine() {return this.line;}

	public int getCol() {return this.col;}

}