package MiniJava.AST;

public class This extends Expression {
	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
	public This(int l, int c){
		super(l,c);
	}

	@Override
	public String toString() {
		return "this";
	}
}
