package MiniJava.AST;

public class Not extends Expression {
	public Expression expr;
	public Not(Expression expr, int l, int c) {
		super(l,c);
		this.expr = expr;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return "~" + "[" + expr + "]";
	}
}