package MiniJava.AST;

public class Print extends SingleStatement {
	public Expression e;
	public Print(Expression e, int l, int c) {
		super(l,c);
		this.e = e;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}	
}