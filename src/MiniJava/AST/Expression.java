package MiniJava.AST;

public abstract class Expression extends Statement {
    public String flattenVariable = "";
    public Expression(int line, int col){
        super(line, col);
    }
};