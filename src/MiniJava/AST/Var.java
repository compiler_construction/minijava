package MiniJava.AST;

public class Var extends Expression {
	public String varID;
	public Var(String varID, int l, int c) {
		super(l,c);
		this.varID = varID;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return varID;
	}
}