package MiniJava.AST;

public class Sidef extends Statement {
	public Expression expression;
	public Sidef(Expression expression, int l, int c) {
		super(l,c);
		this.expression = expression;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
