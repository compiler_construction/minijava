package MiniJava.AST;

public class IntArrayElement extends Var {
	public Expression expression;
	public IntArrayElement(String varID, Expression e, int l, int c) {
		super(varID, l, c);
		expression = e;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return varID.toString() + "[" + expression.toString() + "]";
	}
}
