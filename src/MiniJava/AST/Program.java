package MiniJava.AST;

import java.util.List;

public class Program extends Tree {
	public Class mainClass;
	public List<Class> classes;

	public Program(Class mainClass, List<Class> classes, int l, int c) {
		super(l,c);
		this.mainClass = mainClass;
		this.classes = classes;
	}
	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
