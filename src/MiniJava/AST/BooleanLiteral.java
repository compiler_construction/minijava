package MiniJava.AST;

public class BooleanLiteral extends Expression {
	public boolean value;

	public BooleanLiteral(boolean value, int l, int c)
	{
		super(l,c);
		this.value = value;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return value + "";
	}
}
