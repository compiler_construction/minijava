package MiniJava.AST;

public class Neg extends Expression {
	public Expression expr;
	public Neg(Expression expr, int l, int c) {
		super(l,c);
		this.expr = expr;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
