package MiniJava.AST;
import java.util.List;

public class Class extends Tree {
	public List<VarDecl> varDecls;
	public List<Function> functions;
	public Var id;
	public String parentClass;

	public Class(List<VarDecl> varDecls, List<Function> functions, Var var, String parent, int l, int c) {
		super(l,c);
		this.varDecls = varDecls;
		this.functions = functions;
		this.id = var;
		this.parentClass = parent;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
