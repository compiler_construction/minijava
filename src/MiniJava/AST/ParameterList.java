package MiniJava.AST;

import java.util.List;

public class ParameterList extends Tree {
	public List<VarDecl> parameters;

	public ParameterList(List<VarDecl> parameters, int l, int c) {
		super(l,c);
		this.parameters = parameters;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
