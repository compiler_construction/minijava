package MiniJava.AST;

public class Function extends Tree {
	public Type returnType;
	public Var id;
	public ParameterList parameterList;
	public Block block;
	public boolean isStatic;
	public Function(Type type, Var var, ParameterList parameterList, Block b, int l, int c) {
		super(l,c);
		returnType = type;
		id = var;
		this.parameterList = parameterList;
		block = b;
		isStatic = false;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
