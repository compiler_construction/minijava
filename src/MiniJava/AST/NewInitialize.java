package MiniJava.AST;

public class NewInitialize extends Expression {
	public Var id;

	public NewInitialize(Var id, int l, int c) {
		super(l,c);
		this.id = id;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return "new " + id.varID.toString();
	}
}
