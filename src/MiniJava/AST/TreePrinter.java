package MiniJava.AST;

public class TreePrinter implements Visitor<String> {
	int level = 0;
	void incLevel() {
		level = level + 1;
	}
	void decLevel() {
		level = level - 1;
	}

	String printInc() {
		char[] chars = new char[level];
		java.util.Arrays.fill(chars, '\t');
		return new String(chars);
	}

	public String visit( Var n) {
		return "(ID " + n.varID + ")";
	}

	public String visit( IntLiteral n) {
		return "(INTLIT "+ Integer.toString(n.value) + ")";
	}

	@Override
	public String visit(StringLiteral n) {
		return n.value.toString();
	}

	public String visit( Plus n) {
		return "(+ " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit ( Minus n) {
		return "(- " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( Times n) {
		return "(* " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( Division n) {
		return "(/ " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( Modulo n) {
		return "(% " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( Equals n) {
		return "(== " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( GreaterThan n) {
		return "(> " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( LessThan n) {
		return "(< " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( And n) {
		return "(&& " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( Or n) {
		return "(|| " +n.lhs.accept(this) + n.rhs.accept(this) + ")";
	}

	public String visit( Neg n) {
		return "(- " + n.expr.accept(this) + ")";
	}

	public String visit( Not n) {
		return "(! " + n.expr.accept(this) + ")";
	}

//	public String visit( Identifier)

	// ##############   Statements   ##############

	public String visit( IfThenElse n) {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(printInc()+"(IF " + n.expr.accept(this) + "\n");
		incLevel();
		strBuilder.append(n.then.accept(this));
		decLevel();
		if (n.elze.accept(this) != "") {
			incLevel();
			strBuilder.append(n.elze.accept(this));
			decLevel();
		}
		strBuilder.append(printInc()+")\n");
		return strBuilder.toString();
	}

	public String visit( Print n) {
		return printInc() + "(PRINT " + n.e.accept(this) + "\n";
	}

	public String visit( Assign n) {
		return printInc() + "(" + "EQSIGN " + n.varID.accept(this) +  n.expr.accept(this) + ")\n";
	}

	public String visit( While n) {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append(printInc() + "(WHILE " + n.expr.accept(this) + "\n");
		incLevel();
		strBuilder.append(n.body.accept(this));
		decLevel();
		strBuilder.append(printInc() + ")\n");
		return strBuilder.toString();
	}

	public String visit( For n) {
		StringBuilder strBuilder = new StringBuilder();
		strBuilder.append( printInc() + "for (" + n.init.accept(this).replace("\n"," ") + n.expr.accept(this).replace("\n","") + "; "
				+ n.step.accept(this).replace("\n"," ").replace(";","") + ") {\n");
		incLevel();
		strBuilder.append( n.body.accept(this));
		decLevel();
		strBuilder.append( printInc() + "}\n");
		return strBuilder.toString();
	}

	public String visit( Block n) {
		StringBuilder strBuilder = new StringBuilder();
		for( Statement stat: n.body)
			strBuilder.append(stat.accept(this));
		return strBuilder.toString();
	}

	public String visit( Skip n) {
		return "";
	}

	//for type
	public String visit (Type t) {
		return t.type ;
	}

	@Override
	public String visit(Class v) {
//		String a =  "(CLASS " + v.id.accept(this);
		String a =  "(Class " + v.id.accept(this);
		StringBuilder builder = new StringBuilder(a + "\n");
		incLevel();
		for(Statement varDecl : v.varDecls) {
			builder.append(varDecl.accept(this));
		}
		for(Function function : v.functions) {
			builder.append(function.accept(this));
		}
		decLevel();
		builder.append(")\n");
		return builder.toString();
	}

	@Override
	public String visit(VarDecl varDecl) {
		return printInc() + "(VAR-DECL " + varDecl.type.accept(this) + " " + varDecl.var.accept(this) + ")\n";
	}

	@Override
	public String visit(Function function) {
		StringBuilder stringBuilder = new StringBuilder(printInc() + "(MTD-DECL ");
		stringBuilder.append(function.returnType.accept(this) + " " +
				function.id.accept(this) + " " +
				function.parameterList.accept(this) + "\n");
		stringBuilder.append(printInc() + "(BLOCK\n");
		incLevel();
		stringBuilder.append(function.block.accept(this));
		decLevel();
		stringBuilder.append("\n");
		stringBuilder.append(printInc() + ")\n");
		stringBuilder.append(printInc() + ")\n");
		return stringBuilder.toString();
	}

	@Override
	public String visit(ParameterList parameterList) {
		StringBuilder stringBuilder = new StringBuilder("(TY-ID-LIST ");
		for (VarDecl varDecl : parameterList.parameters) {
			String type = varDecl.type.accept(this);
			String id = varDecl.var.accept(this);
			stringBuilder.append(" (" + type + " " + id + ")");
		}
		stringBuilder.append(")");
		return stringBuilder.toString();
	}

	@Override
	public String visit(Return r) {
		return printInc() + "(RETURN " + r.expression.accept(this) + ")\n";
	}

	@Override
	public String visit(IntArrayElement intArrayElement) {
		return "(ARRAY-LOOKUP "+ intArrayElement.varID + intArrayElement.expression.accept(this) + ")";
	}

	@Override
	public String visit(BooleanLiteral booleanLiteral) {
		return "BOOLEAN "+booleanLiteral.value + "";
	}

	@Override
	public String visit(This t) {
		return "THIS";
	}

	@Override
	public String visit(NewInitialize newInitialize) {
		return "(NEW " + newInitialize.id.accept(this) + ")";
	}

	@Override
	public String visit(NewIntArray newIntArray) {
		return "(NEW-INT-ARRAY "  + newIntArray.expression.accept(this) + ")";

	}

	@Override
	public String visit(ArrayLookup arrayLookup) {
		return "(ARRAY-LOOKUP "+ arrayLookup.id.accept(this) + arrayLookup.lookup.accept(this) + ")";
	}

	@Override
	public String visit(LengthOf lengthOf) {
		return "(LENGTH-OF "+lengthOf.var.accept(this) + ")";
	}

	@Override
	public String visit(Dot dot) {
		StringBuilder builder = new StringBuilder("(DOT "+ dot.expression.accept(this) + dot.id.accept(this));
		for (Expression e : dot.expressions) {
			builder.append(e.accept(this));
		}
		builder.append(")");
		return builder.toString();
	}

	@Override
	public String visit(Program program) {
		StringBuilder builder = new StringBuilder();
		builder.append(program.mainClass.accept(this));
		for (Class c : program.classes) {
			builder.append(c.accept(this));
		}
		return builder.toString();
	}

	@Override
	public String visit(Sidef sidef) {
		return printInc() + "(SIDEF "+ sidef.expression.accept(this) + ")";
	}

	@Override
	public String visit(Yes yes) {
		return null;
	}
}	