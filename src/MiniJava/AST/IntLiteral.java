package MiniJava.AST;

public class IntLiteral extends Expression {
	public int value;
	public IntLiteral( int value, int l, int c) {
		super(l,c);
		this.value = value;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return value + "";
	}
}
