package MiniJava.AST;

public class Return extends Statement {
	public Expression expression;
	public Return(Expression e, int l, int c) {
		super(l,c);
		expression = e;
	}
	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
