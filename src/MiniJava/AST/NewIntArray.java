package MiniJava.AST;

public class NewIntArray extends Expression {
	public Expression expression;

	public NewIntArray(Expression expression, int l, int c) {
		super(l,c);
		this.expression = expression;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return "new int[" + expression.toString() + "]";
	}
}
