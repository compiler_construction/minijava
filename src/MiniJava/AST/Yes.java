package MiniJava.AST;

public class Yes extends Expression {
	public Expression expr;
	public Yes(Expression expr, int l, int c) {
		super(l,c);
		this.expr = expr;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return "" + "[" + expr + "]";
	}
}