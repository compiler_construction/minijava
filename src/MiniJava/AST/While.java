package MiniJava.AST;

public class While extends Statement {
	public Expression expr;
	public Statement body;
	public While(Expression expr, Statement body, int l, int c) {
		super(l,c);
		this.expr = expr; this.body = body;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}