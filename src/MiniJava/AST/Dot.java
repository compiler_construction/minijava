package MiniJava.AST;

import java.util.List;

public class Dot extends Expression {
	public Dot(Expression expression, List<Expression> expressions, Var id, int l, int c) {
		super(l,c);
		this.expression = expression;
		this.expressions = expressions;
		this.id = id;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
	public Expression expression;
	public List<Expression> expressions;
	public Var id;

	@Override
	public String toString() {
		String result = id.toString() + "." + expression.toString() + "(";
		for (Expression para : expressions) {
			result += para.toString() + " ,";
		}
		return result + ")";
	}
}
