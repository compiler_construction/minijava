package MiniJava.AST;

public class ArrayLookup extends Expression {
	public Expression id;
	public Expression lookup;

	public ArrayLookup(Expression id, Expression lookup, int l, int c) {
		super(l,c);
		this.id = id;
		this.lookup = lookup;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return id.toString() + "[" + lookup.toString() + "]";
	}
}
