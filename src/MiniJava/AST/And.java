package MiniJava.AST;

public class And extends Expression {
	public Expression lhs;
	public Expression rhs;
	public And(Expression lhs, Expression rhs, int l, int c) {
		super(l, c);
		this.lhs = lhs; this.rhs = rhs;
	}
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}
}
