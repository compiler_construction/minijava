package MiniJava.AST;

public class LengthOf extends Expression {
	public Expression var;

	public LengthOf(Expression var, int l, int c) {
		super(l,c);
		this.var = var;
	}

	@Override
	public <R> R accept(Visitor<R> v) {
		return v.visit(this);
	}

	@Override
	public String toString() {
		return var.toString() + ".length";
	}
}
