package MiniJava.Syntax;

import MiniJava.AST.*;
import MiniJava.AST.Class;
import MiniJava.Lexer.Token;
import MiniJava.Lexer.Type;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

import static MiniJava.Lexer.Type.*;

public class SyntaxParser {
	private List<Token> tokens;
	private int currentIndex;
	private Token currentToken;
	private Stack<Expression> expressionStack;

	public SyntaxParser(List<Token> tokens) {
		this.tokens = tokens;
		currentIndex = 0;
		currentToken = tokens.get(currentIndex);
		expressionStack = new Stack<>();
	}

	public void nextToken() {
		currentIndex++;
		currentToken = tokens.get(currentIndex);
	}

	public void previousToken() {
		currentIndex--;
		currentToken = tokens.get(currentIndex);
	}

	public List<Class> parseClasses() {
		List<Class> classes = new ArrayList<>();
		parseClasses_Util(classes);
		return classes;
	}

	private void parseClasses_Util(List<Class> classes) {
		switch (currentToken.getType()) {
			case CLASS:
				Class aClass = parseClass();
				classes.add(aClass);
				parseClasses_Util(classes);
				break;
			default:
				break;
		}
	}

	public Program parsing() throws SyntaxParseError {
		Program program = parseProgram();
		parseEOF();
		return program;
	}

	private Program parseProgram() {
		Class main = ParseMainClass();
		List<Class> classes = parseClasses();
		Program program = new Program(main, classes, currentToken.getRow(), currentToken.getColumn());
		return program;
	}

	private Class ParseMainClass() {
		if (currentToken.getType() == Type.CLASS) {
			List<Function> functions = new ArrayList<>();
			nextToken();
			Var id = parseID();
			//{
			nextToken();
			Function f = parseMainFunction();
			functions.add(f);
			//}
			nextToken();
			return new Class(new ArrayList<VarDecl>(), functions, id, "", currentToken.getRow(), currentToken.getColumn());
		} else {
			throw new SyntaxParseError();
		}
	}


	public Expression parseExpression() throws SyntaxParseError {
		parseExpression_2();
		parseExpression_prime();
		return expressionStack.peek();
	}

	private void parseDotOperator() {
		nextToken();
		if (currentToken.getType() == LENGTH) {
			nextToken();
			LengthOf lengthOf = new LengthOf(expressionStack.pop(), currentToken.getRow(), currentToken.getColumn());
			expressionStack.push(lengthOf);
		} else if (currentToken.getType() == ID) {
			Expression expression = expressionStack.pop();
			Var var = parseID();
			//(
			nextToken();
			List<Expression> calling = parseExpressions();
			Dot dot = new Dot(expression, calling, var, currentToken.getRow(), currentToken.getColumn());
			//)
			nextToken();
			expressionStack.push(dot);
		}
	}


	private void parseExpression_2() {
		parseExpression_3();
		parseExpression_2_prime();
	}

	private void parseExpression_3() {
		parseExpression_4();
		parseExpression_3_prime();
	}

	private void parseExpression_3_prime() {
		Expression left = null;
		Expression right = null;
		switch (currentToken.getType()) {
			case EQUALS:
				nextToken();
				parseExpression_4();
				parseExpression_3_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				Expression plus = new Equals(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(plus);
				break;
			case LESSTHAN:
				nextToken();
				parseExpression_4();
				parseExpression_3_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				Expression lessThan = new LessThan(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(lessThan);
				break;
			default:
				parseExpression_4();
				break;
		}
	}

	private void parseExpression_4() {
		parseExpression_5();
		parseExpression_4_prime();
	}

	private void parseExpression_4_prime() {
		Expression left = null;
		Expression right = null;
		switch (currentToken.getType()) {
			case PLUS:
				nextToken();
				parseExpression_5();
				parseExpression_4_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				Expression plus = new Plus(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(plus);
				break;
			case MINUS:
				nextToken();
				parseExpression_5();
				parseExpression_4_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				Expression minus = new Minus(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(minus);
				break;
			default:
				parseExpression_5();
				break;
		}
	}

	private List<Expression> parseExpressions() {
		List<Expression> expressions = new ArrayList<>();
		parseExpressions_Util(expressions);
		return expressions;
	}

	private void parseExpressions_Util(List<Expression> expressions) {
		switch (currentToken.getType()) {
			case LPAREN:
			case ID:
			case INTLIT:
			case STRINGLIT:
			case TRUE:
			case FALSE:
			case THIS:
			case NEW:
			case BANG:
				Expression e = parseExpression();
				expressions.add(e);
				if (currentToken.getType() == COMMA) {
					nextToken();
					parseExpressions_Util(expressions);
				}
		}
	}

	private void parseExpression_5() {
		parseExpression_Final();
		parseExpression_5_prime();
	}

	private void parseExpression_5_prime() {
		Expression result = null;
		Expression left = null, right = null;
		switch (currentToken.getType()) {
			case TIMES:
				nextToken();
				parseExpression_Final();
				parseExpression_5_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				result = new Times(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(result);
				return;
			case DIV:
				nextToken();
				parseExpression_Final();
				parseExpression_5_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				result = new Division(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(result);
				return;
			default:
				parseExpression_Final();
				break;
		}
	}

	private void parseExpression_prime() {
		Expression left = null;
		Expression right = null;
		switch (currentToken.getType()) {
			case OR:
				nextToken();
				parseExpression_2();
				parseExpression_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				Expression or = new Or(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(or);
				break;
			default:
				parseExpression_2();
				break;
		}
	}


	private void parseExpression_2_prime() {
		Expression left = null;
		Expression right = null;
		switch (currentToken.getType()) {
			case AND:
				nextToken();
				parseExpression_3();
				parseExpression_2_prime();
				right = expressionStack.pop();
				left = expressionStack.pop();
				Expression and = new And(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(and);
				return;
			default:
				parseExpression_3();
				break;
		}
	}

	private void parseExpression_other() throws SyntaxParseError {
		Expression result = null;
		switch (currentToken.getType()) {
			case LPAREN:
				nextToken();
				result = parseExpression();
				if (currentToken.getType() != Type.RPAREN) {
					throw new SyntaxParseError();
				} else {
					nextToken();
				}
				break;
			case ID:
			case INTLIT:
			case STRINGLIT:
			case TRUE:
			case FALSE:
			case THIS:
				result = parsePrimitive();
				expressionStack.push(result);
				break;
			case NEW:
				nextToken();
				if (currentToken.getType() == ID) {
					Var id = parseID();
					if (currentToken.getType() == LPAREN) {
						nextToken();
						if (currentToken.getType() == RPAREN) {
							nextToken();
							result = new NewInitialize(id, currentToken.getRow(), currentToken.getColumn());
							expressionStack.push(result);
						} else {
							throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
						}
					} else {
						throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
					}
				} else if (currentToken.getType() == INT) {
					nextToken();
					if (currentToken.getType() == LBRACKET) {
						nextToken();
						Expression expression = parseExpression();
						if (currentToken.getType() == RBRACKET) {
							nextToken();
							expression = expressionStack.pop();
							result = new NewIntArray(expression, currentToken.getRow(), currentToken.getColumn());
							expressionStack.push(result);
						} else {
							throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
						}
					} else {
						throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
					}
				} else {
					throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
				}
				break;
			case BANG:
				nextToken();
				Expression expression = parseExpression();
				expression = expressionStack.pop();
				result = new Not(expression, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(result);
				break;
			case LBRACKET:
				nextToken();
				Expression e = parseExpression();
				if (currentToken.getType() != Type.RBRACKET) {
					throw new SyntaxParseError();
				} else {
					nextToken();
				}
				Expression right = expressionStack.pop();
				Expression left = expressionStack.pop();
				result = new ArrayLookup(left, right, currentToken.getRow(), currentToken.getColumn());
				expressionStack.push(result);
				break;
			case DOT:
				parseDotOperator();
				break;
			default:
		}
	}

	private void parseExpression_Final() throws SyntaxParseError {
		parseExpression_other();
	}

	private void parseSemiColon() throws SyntaxParseError {
		if (currentToken.getType() == Type.SEMICOLON) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseEOF() throws SyntaxParseError {
		if (currentToken.getType() == Type.EOF) {
			return;
		} else {
			throw new SyntaxParseError();
		}
	}

	public Statement parseStatement() throws SyntaxParseError {
		Statement result = null;
		switch (currentToken.getType()) {
			case IF:
				int row = currentToken.getRow();
				int col = currentToken.getColumn();
				nextToken();
				if (currentToken.getType() == Type.LPAREN) {
					nextToken();
					Expression expression = parseExpression();
					if (currentToken.getType() == Type.RPAREN) {
						nextToken();
						Statement matched = parseMatched();
						Statement optionalTail = parseOptionalTail();
						result = new IfThenElse(expression, matched, optionalTail, row, col);
					} else {
						throw new SyntaxParseError();
					}
				} else {
					throw new SyntaxParseError();
				}
				return result;
			default:
				//other cases
				Statement other = parseOther();
				return other;
		}
	}

	private Statement parseOther() throws SyntaxParseError {
		Statement result = null;
		Expression expression = null;
		switch (currentToken.getType()) {
			case LBRACE:
				nextToken();
				List<Statement> statements = parseStatements();
				result = new Block(statements, currentToken.getRow(), currentToken.getColumn());
				if (currentToken.getType() == Type.RBRACE) {
					nextToken();
				} else {
					throw new SyntaxParseError();
				}
				return result;
			//need to change
			case ID:
				Var id = new Var(currentToken.getValue(), currentToken.getRow(), currentToken.getColumn());
				nextToken();
				if (currentToken.getType() == Type.LBRACKET) {
					nextToken();
					Expression e = parseExpression();
					if (currentToken.getType() == Type.RBRACKET) {
						IntArrayElement intArrayElement = new IntArrayElement(id.varID, e, currentToken.getRow(), currentToken.getColumn());
						nextToken();
						if (currentToken.getType() == Type.EQSIGN) {
							nextToken();
							expression = parseExpression();
							parseSemiColon();
							result = new Assign(intArrayElement, expression, currentToken.getRow(), currentToken.getColumn());
							return result;
						} else {
							throw new SyntaxParseError();
						}
					} else {
						throw new SyntaxParseError();
					}
				} else if (currentToken.getType() == Type.EQSIGN) {
					nextToken();
					expression = parseExpression();
					parseSemiColon();
					result = new Assign(id, expression, currentToken.getRow(), currentToken.getColumn());
				} else {
					throw new SyntaxParseError();
				}
				return result;
			case PRINTLN:
				nextToken();
				parseLeftParenthesis();
				expression = parseExpression();
				//might need to change
				result = new Print(expression, currentToken.getRow(), currentToken.getColumn());
				parseRightParenthesis();
				parseSemiColon();
				return result;
			case WHILE:
				nextToken();
				if (currentToken.getType() == Type.LPAREN) {
					nextToken();
					expression = parseExpression();
					if (currentToken.getType() == Type.RPAREN) {
						nextToken();
						Statement body = parseStatement();
						result = new While(expression, body, currentToken.getRow(), currentToken.getColumn());
						return result;
					} else {
						throw new SyntaxParseError();
					}
				} else {
					throw new SyntaxParseError();
				}
			case SIDEF:
				nextToken();
				if (currentToken.getType() == Type.LPAREN) {
					nextToken();
					expression = parseExpression();
					if (currentToken.getType() == Type.RPAREN) {
						nextToken();
						result = new Sidef(expression, currentToken.getRow(), currentToken.getColumn());
						parseSemiColon();
						return result;
					} else {
						throw new SyntaxParseError();
					}
				} else {
					throw new SyntaxParseError();
				}
			default:
				return result;
		}
	}

	private Statement parseOptionalTail() {
		Statement statement = null;
		switch (currentToken.getType()) {
			case ELSE:
				nextToken();
				Statement tail = parseTail();
				return tail;
			default:
				return new Skip(currentToken.getRow(), currentToken.getColumn());
		}
	}

	private Statement parseTail() {
		Statement statement = null;
		switch (currentToken.getType()) {
			case IF:
				nextToken();
				if (currentToken.getType() == Type.LPAREN) {
					nextToken();
					Expression expression = parseExpression();
					if (currentToken.getType() == Type.RPAREN) {
						nextToken();
						Statement tail = parseTail();
						statement = new IfThenElse(expression, tail, new Skip(currentToken.getRow(), currentToken.getColumn()), currentToken.getRow(), currentToken.getColumn());
					} else {
						throw new SyntaxParseError();
					}
				} else {
					throw new SyntaxParseError();
				}
				return statement;
			default:
				return parseOther();
		}
	}

	private Statement parseMatched() throws SyntaxParseError {
		Statement statement = null;
		switch (currentToken.getType()) {
			case IF:
				nextToken();
				if (currentToken.getType() == Type.LPAREN) {
					nextToken();
					Expression expression = parseExpression();
					if (currentToken.getType() == Type.RPAREN) {
						nextToken();
						Statement matched_1 = parseMatched();
						if (currentToken.getType() == Type.ELSE) {
							nextToken();
							Statement matched_2 = parseMatched();
							statement = new IfThenElse(expression, matched_1, matched_2, currentToken.getRow(), currentToken.getColumn());
						} else {
							throw new SyntaxParseError();
						}
					} else {
						throw new SyntaxParseError();
					}
				} else {
					throw new SyntaxParseError();
				}
				return statement;
			default:
				//other cases
//				return parseOther();
				return parseStatement();
		}
	}


	private Class parseClass() {
		if (currentToken.getType() == Type.CLASS) {
			nextToken();
			String parent = "";
			int start_line = currentToken.getRow();
			int start_column = currentToken.getColumn();
			Var id = parseID();
			//{ or extends
			switch (currentToken.getType()) {
				case LBRACE:
					nextToken();
					break;
				case EXTENDS:
					nextToken();
					if (currentToken.getType() == Type.ID) {
						parent = currentToken.getValue();
						nextToken();
						nextToken();
					} else {
						throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
					}
					break;
				default:
					throw new SyntaxParseError("Error: " + currentToken.getRow() + "," + currentToken.getColumn());
			}
			List<VarDecl> varDecls = parseVarDecls();
			List<Function> functions = parseFunctions();
			//}
			nextToken();
			return new Class(varDecls, functions, id, parent, start_line, start_column);
		} else {
			throw new SyntaxParseError();
		}
	}

	private List<Function> parseFunctions() {
		List<Function> functions = new ArrayList<>();
		parseFunctions_Util(functions);
		return functions;
	}

	private void parseFunctions_Util(List<Function> functions) {
		switch (currentToken.getType()) {
			case PUBLIC:
				Function function = parseFunction();
				functions.add(function);
				parseFunctions_Util(functions);
				break;
			default:
				break;
		}
	}

	private VarDecl parseVarDeclare() {
		MiniJava.AST.Type t = parseType();
		int start_line = currentToken.getRow();
		int start_column = currentToken.getColumn();
		Var id = parseID();
		parseSemiColon();
		return new VarDecl(t, id, start_line, start_column);
	}

	private Var parseID() {
		if (currentToken.getType() == Type.ID) {
			Var id = new Var(currentToken.getValue(), currentToken.getRow(), currentToken.getColumn());
			nextToken();
			return id;
		} else {
			throw new SyntaxParseError();
		}
	}

	public Expression parsePrimitive() throws SyntaxParseError {
		Expression result = null;
		switch (currentToken.getType()) {
			case ID:
				result = new Var(currentToken.getValue(), currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return result;
			case INTLIT:
				result = new IntLiteral(Integer.parseInt(currentToken.getValue()), currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return result;
			case STRINGLIT:
				result = new StringLiteral(currentToken.getValue(), currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return result;
			case TRUE:
				result = new BooleanLiteral(true, currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return result;
			case FALSE:
				result = new BooleanLiteral(false, currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return result;
			case THIS:
				result = new This(currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return result;
			default:
				throw new SyntaxParseError();
		}
	}

	public void parseLeftParenthesis() throws SyntaxParseError {
		if (currentToken.getType() == Type.LPAREN) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseRightParenthesis() throws SyntaxParseError {
		if (currentToken.getType() == Type.RPAREN) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parsePublic() throws SyntaxParseError {
		if (currentToken.getType() == Type.PUBLIC) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseStatic() throws SyntaxParseError {
		if (currentToken.getType() == Type.STATIC) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseVoid() throws SyntaxParseError {
		if (currentToken.getType() == Type.VOID) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseMain() throws SyntaxParseError {
		if (currentToken.getType() == Type.MAIN) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseLeftBrace() throws SyntaxParseError {
		if (currentToken.getType() == Type.LBRACE) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseRightBrace() throws SyntaxParseError {
		if (currentToken.getType() == Type.RBRACE) {
			nextToken();
		} else {
			throw new SyntaxParseError();
		}
	}

	public void parseStringArray() throws SyntaxParseError {
		if (currentToken.getType() == Type.STRING) {
			nextToken();
			// expect a [
			if (currentToken.getType() == Type.LBRACKET) {
				nextToken();
				// expect a ]
				if (currentToken.getType() == Type.RBRACKET) {
					nextToken();
				} else {
					throw new SyntaxParseError();
				}
			} else {
				throw new SyntaxParseError();
			}
		} else {
			throw new SyntaxParseError();
		}
	}

	public MiniJava.AST.Type parseType() throws SyntaxParseError {
		MiniJava.AST.Type type = null;
		switch (currentToken.getType()) {
			case INT:
				nextToken();
				if (currentToken.getType() == Type.LBRACKET) {
					nextToken();
					// expect a ]
					if (currentToken.getType() == Type.RBRACKET) {
						nextToken();
						type = new MiniJava.AST.Type("INT-ARRAY", currentToken.getRow(), currentToken.getColumn());
					} else {
						throw new SyntaxParseError();
					}
				} else {
					type = new MiniJava.AST.Type(INT.toString(), currentToken.getRow(), currentToken.getColumn());
				}
				return type;
			case BOOLEAN:
				nextToken();
				type = new MiniJava.AST.Type(BOOLEAN.toString(), currentToken.getRow(), currentToken.getColumn());
				return type;
			case STRING:
				nextToken();
				if (currentToken.getType() == Type.LBRACKET) {
					nextToken();
					// expect a ]
					if (currentToken.getType() == Type.RBRACKET) {
						nextToken();
						type = new MiniJava.AST.Type("STRING-ARRAY", currentToken.getRow(), currentToken.getColumn());
					} else {
						throw new SyntaxParseError();
					}
				} else {
					type = new MiniJava.AST.Type(STRING.toString(), currentToken.getRow(), currentToken.getColumn());
				}
				return type;
			case ID:
				type = new MiniJava.AST.Type(currentToken.getValue(), currentToken.getRow(), currentToken.getColumn());
				nextToken();
				return type;
			default:
				throw new SyntaxParseError();
		}
	}

	public ParameterList parseParameters() throws SyntaxParseError {
		if (currentToken.getType() == Type.LPAREN) {
			List<VarDecl> parameters = new ArrayList<>();
			ParameterList parameterList = new ParameterList(parameters, currentToken.getRow(), currentToken.getColumn());
			nextToken();
			parseParameterUtil(parameters);
			if (currentToken.getType() != Type.RPAREN) {
				throw new SyntaxParseError();
			}
			nextToken();
			return parameterList;
		} else {
			throw new SyntaxParseError();
		}
	}

	public List<VarDecl> parseVarDecls() throws SyntaxParseError {
		List<VarDecl> varDecls = new ArrayList<>();
		parseVarDeclsUtil(varDecls);
		return varDecls;
	}

	private void parseVarDeclsUtil(List<VarDecl> varDecls) {
		switch (currentToken.getType()) {
			case INT:
			case BOOLEAN:
			case STRING:
				VarDecl varDecl = parseVarDeclare();
				varDecls.add(varDecl);
				parseVarDeclsUtil(varDecls);
				return;
			case ID:
				nextToken();
				if (currentToken.getType() != ID) {
					previousToken();
					return;
				} else {
					previousToken();
					varDecl = parseVarDeclare();
					varDecls.add(varDecl);
					parseVarDeclsUtil(varDecls);
					return;
				}
			default:
				return;
		}
	}

	private void parseParameterUtil(List<VarDecl> parameters) {
		switch (currentToken.getType()) {
			case INT:
			case BOOLEAN:
			case STRING:
			case ID:
				MiniJava.AST.Type t = parseType();
				Var id = parseID();
				VarDecl varDecl = new VarDecl(t, id, currentToken.getRow(), currentToken.getColumn());
				parameters.add(varDecl);
				if (currentToken.getType() == Type.COMMA) {
					nextToken();
					parseParameterUtil(parameters);
				} else if (currentToken.getType() == Type.RPAREN) {
					return;
				}
			default:
				return;
		}
	}

	private Function parseFunction() {
		parsePublic();
		MiniJava.AST.Type type = parseType();
		int start_line = currentToken.getRow();
		int start_column = currentToken.getColumn();
		Var id = parseID();
		ParameterList parameters = parseParameters();
		parseLeftBrace();
		List<Statement> inside = new ArrayList<>();
		Block block = new Block(inside, currentToken.getRow(), currentToken.getColumn());
		List<VarDecl> varDecls = parseVarDecls();
		inside.addAll(varDecls);
		List<Statement> statements = parseStatements();
		inside.addAll(statements);
		Return r = parseReturn();
		inside.add(r);
		parseRightBrace();
		Function function = new Function(type, id, parameters, block, start_line, start_column);
		return function;
	}

	private Function parseMainFunction() {
		parsePublic();
		parseStatic();
		parseVoid();
		MiniJava.AST.Type type = new MiniJava.AST.Type("VOID", currentToken.getRow(), currentToken.getColumn());
		parseMain();
		int start_line = currentToken.getRow();
		int start_column = currentToken.getColumn();
		Var id = new Var("MAIN-FUN_CALL", currentToken.getRow(), currentToken.getColumn());
		ParameterList parameters = parseParameters();
		parseLeftBrace();
		List<Statement> inside = new ArrayList<>();
		Block block = new Block(inside, currentToken.getRow(), currentToken.getColumn());
		List<VarDecl> varDecls = parseVarDecls();
		inside.addAll(varDecls);
		List<Statement> statements = parseStatements();
		inside.addAll(statements);
		parseRightBrace();
		Function function = new Function(type, id, parameters, block, start_line, start_column);
		function.isStatic = true;
		return function;
	}

	private List<Statement> parseStatements() {
		List<Statement> statements = new ArrayList<>();
		parseStatementsUtil(statements);
		return statements;
	}

	private void parseStatementsUtil(List<Statement> statements) {
		switch (currentToken.getType()) {
			case IF:
			case LBRACE:
			case PRINTLN:
			case WHILE:
			case SIDEF:
			case ID:
				Statement statement = parseStatement();
				statements.add(statement);
				parseStatementsUtil(statements);
				return;
			default:
				return;
		}
	}

	private Return parseReturn() throws SyntaxParseError {
		if (currentToken.getType() == Type.RETURN) {
			nextToken();
			Expression expression = parseExpression();
			parseSemiColon();
			return new Return(expression, currentToken.getRow(), currentToken.getColumn());
		} else {
			throw new SyntaxParseError();
		}
	}
}
