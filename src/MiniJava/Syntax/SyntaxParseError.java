package MiniJava.Syntax;

public class SyntaxParseError extends RuntimeException {
	public SyntaxParseError() {
		super();
	}

	public SyntaxParseError(String msg) {
		super(msg);
	}
}
