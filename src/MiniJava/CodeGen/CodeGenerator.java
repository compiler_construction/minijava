package MiniJava.CodeGen;

import MiniJava.AST.*;
import MiniJava.AST.Class;
import MiniJava.CodeGen.Node;
import com.sun.org.apache.xpath.internal.operations.Variable;
import javafx.util.Pair;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.*;

public class CodeGenerator implements Visitor<Node> {

    public int num_code_generated = 0;
    Stack<Pair<String, String>> undoStack = new Stack<Pair<String, String>>();
    //This is for the current environment
    List<String> files = new ArrayList<>();
    FileWriter writer;
    PrintWriter printer;
    Map<String, String> symbolTable = new HashMap<String, String>();
    Stack<Integer> scopeMarkers = new Stack<Integer>();
    Set<String> classSet = new HashSet<>();
    Map<String, Class> classMap = new HashMap<>();
    //This is for field overriding check
    Map<String, List<Pair<String, String>>> classMemberVariable = new HashMap<>();
    Map<String, List<String>> functionMemberVariable = new HashMap<>();
    //This is for function overriding
    Map<String, List<Function>> classMemberfunction = new HashMap<>();
    Map<String, Integer> LocalVarNo = new HashMap<>();


    boolean isStatic = false;

    boolean isClassVisited = false;

    boolean isPrintLn = false;

    //for return
    Stack<Pair<String, String>> functionStack = new Stack<>();
    //for this
    Stack<String> classStack = new Stack<>();

    int load_counter = 1;
    int local_var_counter = 0;

    public CodeGenerator() {
        classSet.add("INT");
        classSet.add("BOOLEAN");
        classSet.add("STRING");
        classSet.add("INT-ARRAY");
    }

    public void enterScope() {
        scopeMarkers.push(undoStack.size());
        return;
    }

    public void exitScope() {
        int marker = scopeMarkers.pop();
        while (undoStack.size() > marker) {
            undoStack.pop();
        }
        Stack<Pair<String, String>> copyStack = new Stack<>();
        copyStack = (Stack<Pair<String, String>>) undoStack.clone();
        //clear the table first
        symbolTable.clear();
        //contruct the table
        while (copyStack.size() > 0) {
            Pair<String, String> pair = copyStack.pop();
            symbolTable.put(pair.getKey(), pair.getValue());
        }
    }

    public boolean checkIfDefined(String id) {
        for (Map.Entry<String, String> key : symbolTable.entrySet()) {
            if (key.getKey().equals(id))
                return true;
        }
        return false;
    }

    public String getType(String id) {
        String type = "";
        String cclass = id;
        if (classMap.containsKey(cclass)) {
            while (!classMap.get(cclass).parentClass.equals("")) {
                List<Function> fl = classMemberfunction.get(classStack.peek());
                for (Function f : fl) {
                    if (f.id.varID.equals(id)) {
                        return f.returnType.type;
                    }
                }
                cclass = classMap.get(cclass).parentClass;
            }
        } else {
            if (symbolTable.containsKey(id)) {
                return symbolTable.get(id);
            }
        }
        return type;
    }

    @Override
    public Node visit(IntArrayElement intArrayElement) {
        Node node = new Node();
        node = intArrayElement.expression.accept(this);
        node.res &= checkIfDefined(intArrayElement.varID);
        node.type = getType(intArrayElement.varID);
        return node;
    }

    @Override
    public Node visit(BooleanLiteral booleanLiteral) {
        Node n = new Node();
        n.type = "BOOLEAN";
        if (isClassVisited) {
            if (booleanLiteral.value == true)
                n.value = "1";
            else
                n.value = "0";
            printer.append("\n");
            printer.append("b" + getPrefix(n.type) + "push " + n.value);
            num_code_generated++;
        }
        return n;
    }

    public String getStringType(String type) {
        String typehandle = "";
        if (type.equals("INT"))
            typehandle = "I";
        else if (type.equals("BOOLEAN"))
            typehandle = "I";
        else if (type.equals("STRING"))
            typehandle = "Ljava/lang/String;";
        else if (type.equals("INT-ARRAY"))
            typehandle = "[I";
        else if (type.equals("STRING-ARRAY"))
            typehandle = "[Ljava/lang/String;";
        else if (type.equals("VOID"))
            typehandle = "V";
        else
            typehandle = "L"+type+";";
        return typehandle;
    }

    @Override
    public Node visit(ParameterList parameterList) {
        if (isClassVisited) {
            Map<String, String> parameterSymbolTable = new HashMap<>();
            printer.append("(");
            for (VarDecl varDecl : parameterList.parameters) {
                if (parameterSymbolTable.containsKey(varDecl.var.varID)) {
                    System.err.println(varDecl.getLine() + 1 + ":" + (varDecl.getCol() + 1) + " error: '" +
                            varDecl.var.varID + "' is already defined");
                } else {
                    parameterSymbolTable.put(varDecl.var.varID, varDecl.type.type);
                    undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
                    printer.append(getStringType(varDecl.type.type));
                }
            }
            printer.append(")");
            symbolTable.putAll(parameterSymbolTable);
        }
        return null;
    }

    @Override
    public Node visit(NewInitialize newInitialize) {
        Node node = new Node();
        node = newInitialize.id.accept(this);
        printer.append("\nnew " + node.value);
        printer.append("\ndup");
        printer.append("\ninvokespecial " + node.value + "/<init>()V");
        node.type = node.value;
        num_code_generated++;
        return node;
    }

    @Override
    public Node visit(NewIntArray newIntArray) {
        Node node = new Node();
        node = newIntArray.expression.accept(this);
        getOrLoad(node);
        num_code_generated++;
        printer.append("\nnewarray "+node.type.toLowerCase());
        return node;
    }

    @Override
    public Node visit(ArrayLookup arrayLookup) {
        Node node = new Node();
        node = arrayLookup.id.accept(this);
        getOrLoad(node);
        Node anode = arrayLookup.lookup.accept(this);
        getOrLoad(anode);
        node.res &= anode.res;
        node.type = "INT";
        printer.append("\niaload");
        num_code_generated++;
        return node;
    }

    @Override
    public Node visit(VarDecl varDecl) {
        undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
        symbolTable.put(varDecl.var.varID, varDecl.type.type);
        varDecl.var.accept(this);
        String val = "";
        if (varDecl.type.type.equals("INT"))
            val = "0";
        else if (varDecl.type.type.equals("BOOLEAN"))
            val = "1";
        if (isClassVisited && functionStack.isEmpty()) {
            //LocalVarNo.put(varDecl.var.varID, ++local_var_counter);
            printer.append("\n.field " + varDecl.var.varID + " " + getStringType(varDecl.type.type));
        }
        num_code_generated++;
        return null;
    }

    @Override
    public Node visit(LengthOf lengthOf) {
        Node node = new Node();
        node = lengthOf.var.accept(this);
        getOrLoad(node);
        printer.append("\narraylength");
        return node;
    }

    @Override
    public Node visit(Function function) {
        LocalVarNo.clear();
        load_counter = 1;
        int locals = 10;
        int stack_var = 10;
        isStatic = function.isStatic;
        if (!function.isStatic)
            locals += 1;
        if (isClassVisited) {
            printer = new PrintWriter(writer, true);
            String staticNonStatic = function.isStatic ? "static " : "";
            printer.append("\n.method public " + staticNonStatic);
            if (function.id.varID.equals("MAIN-FUN_CALL"))
                printer.append("main");
            else
                printer.append(function.id.varID);
            isStatic = function.isStatic;
            //Shadowing is allowed after this
            enterScope();
            //Symbol table in the function scope, you cannot shadow local variables inside function scope
            Map<String, String> VarSymbolTable = new HashMap<>();
            function.parameterList.accept(this);
            printer.append(getStringType(function.returnType.type));
            printer.append("\n.limit locals " + locals);
            printer.append("\n.limit stack " + stack_var);


            //Store local variables
            for (VarDecl varDecl : function.parameterList.parameters) {
                //Pop locals and push onto stack
                if (functionMemberVariable.containsKey(function.id.varID)) {
                    functionMemberVariable.get(function.id.varID).listIterator().add(varDecl.var.varID);
                    LocalVarNo.put(varDecl.var.varID,load_counter);
                    load_counter++;
                }
            }
            for (Statement statement : function.block.body) {
                if (statement instanceof VarDecl) {
                    VarDecl varDecl = (VarDecl) statement;
                    if (VarSymbolTable.containsKey(varDecl.var.varID)) {
                        System.err.println(varDecl.getLine() + 1 + ":" + (varDecl.getCol() + 1) + " error: '" +
                                varDecl.var.varID + "' is already defined");
                    }
                    VarSymbolTable.put(varDecl.var.varID, varDecl.type.type);
                    symbolTable.put(varDecl.var.varID, varDecl.type.type);
                    undoStack.push(new Pair<>(varDecl.var.varID, varDecl.type.type));
                    if (functionMemberVariable.containsKey(function.id.varID)) {
                        functionMemberVariable.get(function.id.varID).listIterator().add(varDecl.var.varID);
                        LocalVarNo.put(varDecl.var.varID,load_counter);
                        load_counter++;
                    }
                }
                statement.accept(this);
            }
            if (!functionStack.empty())
                functionStack.pop();
            if (function.returnType.type.equals("VOID"))
                printer.append("\nreturn");
            printer.append("\n.end method");
            exitScope();
        }
        return null;
    }

    @Override
    public Node visit(StringLiteral n) {
        Node node = new Node();
        node.type = "STRING";
        node.value = n.value;
        if (isClassVisited) {
            printer.append("\nldc " + node.value);
            num_code_generated++;
        }
        return node;
    }

    @Override
    public Node visit(Program program) {
        num_code_generated = 0;
        undoStack.push(new Pair<>(program.mainClass.id.varID, "Class"));
        symbolTable.put(program.mainClass.id.varID, "Class");
        classSet.add(program.mainClass.id.varID);
        classMap.put(program.mainClass.id.varID, program.mainClass);
        for (Class c : program.classes) {
            undoStack.push(new Pair<>(c.id.varID, "Class"));
            symbolTable.put(c.id.varID, "Class");
            classSet.add(c.id.varID);
            classMap.put(c.id.varID, c);
            classMemberVariable.put(c.id.varID, new ArrayList<>());
            classMemberfunction.put(c.id.varID, new ArrayList<>());
            classMemberfunction.put("out", new ArrayList<>());
        }
        for (Class c : program.classes) {
            classStack.push(c.id.varID);
            c.accept(this);
        }
        isClassVisited = true;
        for (Class c : program.classes) {
            classStack.push(c.id.varID);
            File file = new File("" + c.id.varID + ".j");
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            files.add("" + c.id.varID + ".j");
            c.accept(this);
        }
        classStack.push(program.mainClass.id.varID);
        File file = new File("" + program.mainClass.id.varID + ".j");
        try {
            file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        files.add("" + program.mainClass.id.varID + ".j");
        program.mainClass.accept(this);
        return null;
    }

    @Override
    public Node visit(GreaterThan n) {
        Node node = new Node();
        //lhs
        Node lnode = n.lhs.accept(this);
        getOrLoad(lnode);
        //rhs
        Node rnode = n.rhs.accept(this);
        getOrLoad(rnode);
        //root
        node.type = "BOOLEAN";
        node.value = "\nif_" + getPrefix(lnode.type) + "cmpgt";
        num_code_generated++;
        return node;
    }

    @Override
    public Node visit(IntLiteral n) {
        Node node = new Node();
        node.value = "" + n.value;
        node.type = "INT";
        if (isClassVisited) {
            printer.append("\n");
            printer.append("b" + getPrefix(node.type) + "push " + node.value);
        }
        return node;
    }

    @Override
    public Node visit(IfThenElse n) {
        Node cnode = n.expr.accept(this);
        if (cnode.type.equals("BOOLEAN"))
            printer.append(cnode.value);
        else
            printer.append("\nifne");
        printer.append(" nTrue" + "\ngoto nFalse");
        printer.append("\nnTrue:");
        Node tnode = n.then.accept(this);
        printer.append("\nnFalse:");
        Node enode = n.elze.accept(this);
        return null;
    }

    @Override
    public Node visit(Sidef sidef) {
        Node node = sidef.expression.accept(this);
        return node;
    }

    @Override
    public Node visit(Yes yes) {
        return null;
    }

    @Override
    public Node visit(LessThan n) {
        Node node = new Node();
        //lhs
        Node lnode = n.lhs.accept(this);
        getOrLoad(lnode);
        //rhs
        Node rnode = n.rhs.accept(this);
        getOrLoad(rnode);
        //root
        node.type = "BOOLEAN";
        node.value = "\nif_" + getPrefix(lnode.type) + "cmplt";
        num_code_generated++;
        return node;
    }

    @Override
    public Node visit(Print n) {
        Node node = new Node();
        isPrintLn = true;
        printer.append("\ngetstatic java/lang/System/out Ljava/io/PrintStream;");
        node = n.e.accept(this);
        printer.append("\ninvokevirtual java/io/PrintStream/print" + "(");
        printer.append(getStringType(node.type));
        printer.append(")" + "V");
        num_code_generated++;
        num_code_generated++;
        isPrintLn = false;
        return node;
    }

    @Override
    public Node visit(Division n) {
        //lhs
        Node lnode = new Node();
        lnode = n.lhs.accept(this);
        if (symbolTable.containsKey(lnode.value))
            getOrLoad(lnode);
        //rhs
        Node rnode = new Node();
        rnode = n.rhs.accept(this);
        if (symbolTable.containsKey(rnode.value))
            getOrLoad(rnode);
        //root
        printer.append("\n" + getPrefix(lnode.type) + "div");
        num_code_generated++;
        return null;
    }

    @Override
    public Node visit(Return r) {
        Node node = new Node();
        if (isClassVisited) {
            node = r.expression.accept(this);
            getOrLoad(node);
            printer.append("\n");
            if (node.type.equals("INT") || node.type.equals("BOOLEAN") || node.type.equals("INT-ARRAY"))
                printer.append("ireturn");
            else if (node.type.equals("STRING"))
                printer.append("Ljava/lang/String;");
            else if (node.type.equals("VOID"))
                printer.append("return");
        }
        num_code_generated++;
        return node;
    }

    @Override
    public Node visit(Modulo n) {
        return null;
    }

    @Override
    public Node visit(Equals n) {

        Node node = new Node();
        //lhs
        Node lnode = n.lhs.accept(this);
        getOrLoad(lnode);
        //rhs
        Node rnode = n.rhs.accept(this);
        getOrLoad(rnode);
        //root
        node.type = "BOOLEAN";
        node.value = "\nif_" + getPrefix(lnode.type) + "cmpeq";
        num_code_generated++;
        return node;
    }

    public String getPrefix(String type) {
        if (type.equals("INT"))
            return "i";
        else if (type.equals("BOOLEAN"))
            return "i";
        else if (type.equals("STRING"))
            return "/java/lang/String";
        return "";
    }

    public boolean checkIfLocalOrInstance(Node node) {
        for (Map.Entry<String, List<String>> key : functionMemberVariable.entrySet()) {
            for (String s : key.getValue()) {
                if (s.equals(node.value)) {
                    return true;
                }
            }
        }
        return false;
    }

    public void putOrStore(Node node) {
        if (checkIfLocalOrInstance(node)) {
            printer.append("\n" + getPrefix(node.type) + "store_" + LocalVarNo.get(node.value));
        } else
            printer.append("\n" + "putfield " + classStack.peek() + "/" + node.value + " " + getStringType(node.type));
        num_code_generated++;
    }

    public void getOrLoad(Node node) {
        if (!isStatic && !checkIfLocalOrInstance(node) && symbolTable.containsKey(node.value))
            printer.append("\naload_0");
        if (checkIfLocalOrInstance(node) && classMap.containsKey(node.type)) {
            printer.append("\n" + getPrefix(node.type) + "aload_" + LocalVarNo.get(node.value));
        }else if (checkIfLocalOrInstance(node)) {
            printer.append("\n" + getPrefix(node.type) + "load_" + LocalVarNo.get(node.value));
        } else if (!checkIfLocalOrInstance(node) && symbolTable.containsKey(node.value))
            printer.append("\n" + "getfield " + classStack.peek() + "/" + node.value + " " + getStringType(node.type));
        num_code_generated++;
    }

    @Override
    public Node visit(Assign n) {
        Node node = new Node();
        Node lnode = new Node();
        Node rnode = new Node();
        lnode = n.varID.accept(this);
        if (!isStatic && !checkIfLocalOrInstance(lnode))
            printer.append("\naload_0");
        //lhs
        //no need to care as it wont be used on stack
        lnode = n.varID.accept(this);
        //rhs, this will be used on stack
        rnode = n.expr.accept(this);
        //root
        putOrStore(lnode);
        num_code_generated++;
        return node;
    }

    @Override
    public Node visit(While n) {
        printer.append("\nnBegin:");
        Node cnode = n.expr.accept(this);
        printer.append(cnode.value);
        printer.append(" nContinue");
        printer.append("\nnContinue:");
        Node tnode = n.body.accept(this);
        printer.append("\ngoto nBegin");
        printer.append("\nnExit:");
        num_code_generated++;
        num_code_generated++;
        num_code_generated++;
        return null;
    }

    @Override
    public Node visit(Times n) {
        //lhs
        Node lnode = new Node();
        lnode = n.lhs.accept(this);
        if (symbolTable.containsKey(lnode.value))
            getOrLoad(lnode);
        //rhs
        Node rnode = new Node();
        rnode = n.rhs.accept(this);
        if (symbolTable.containsKey(rnode.value))
            getOrLoad(rnode);
        //root
        printer.append("\n" + getPrefix(lnode.type) + "mul");
        num_code_generated++;
        return null;
    }

    @Override
    public Node visit(Minus n) {
        //lhs
        Node lnode = new Node();
        lnode = n.lhs.accept(this);
        if (symbolTable.containsKey(lnode.value))
            getOrLoad(lnode);
        //rhs
        Node rnode = new Node();
        rnode = n.rhs.accept(this);
        if (symbolTable.containsKey(rnode.value))
            getOrLoad(rnode);
        //root
        printer.append("\n" + getPrefix(lnode.type) + "minus");
        num_code_generated++;
        return null;
    }

    @Override
    public Node visit(Dot dot) {
        Node node = new Node();
        Boolean b = true;
        if (isClassVisited) {
            Node cnode = dot.expression.accept(this);
            getOrLoad(cnode);
            List<Function> fl = classMemberfunction.get(cnode.type);
            ListIterator<Function> itr = fl.listIterator();
            while (itr.hasNext()) {
                Function f = itr.next();
                if (f.id.varID.equals(dot.id.varID)) {
                    Function fn = f;
                    isStatic = fn.isStatic;
                    if (!isStatic) {
                        Node pnode = new Node();
                        for (Expression e : dot.expressions) {
                            pnode = e.accept(this);
                            b &= pnode.res;
                        }
                        printer.append("\ninvokevirtual " + cnode.type + "/");
                    } else {
                        Node pnode = new Node();
                        for (Expression e : dot.expressions) {
                            pnode = e.accept(this);
                            b &= pnode.res;
                        }
                        printer.append("\ninvokestatic ");
                    }
                    num_code_generated++;
                    printer.append(dot.id.varID);
                    printer.append("(");
                    symbolTable.put(fn.id.varID, fn.returnType.type);
                    if (f.parameterList.parameters.size() == dot.expressions.size()) {
                        if (f.parameterList.parameters.size() == 0) {
                            undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                            if (!functionStack.peek().getKey().equals(fn.id.varID))
                                functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                            symbolTable.put(fn.id.varID, fn.returnType.type);
                        } else {
                            for (VarDecl v : fn.parameterList.parameters) {
                                printer.append(getStringType(v.type.type));
                            }
                        }
                        for (int i = 0; i < f.parameterList.parameters.size(); i++) {
                            undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                            symbolTable.put(fn.id.varID, fn.returnType.type);
                            if (!functionStack.peek().getKey().equals(fn.id.varID))
                                functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                        }
                    }
                    break;
                }
            }
            while (classMap.get(cnode.type).parentClass != "" && !symbolTable.containsKey(dot.id.varID)) {
                fl = classMemberfunction.get(classMap.get(cnode.type).parentClass);
                itr = fl.listIterator();
                while (itr.hasNext()) {
                    Function f = itr.next();
                    if (f.id.varID.equals(dot.id.varID)) {
                        Function fn = f;
                        isStatic = fn.isStatic;
                        if (!isStatic) {
                            Node pnode = new Node();
                            for (Expression e : dot.expressions) {
                                pnode = e.accept(this);
                                b &= pnode.res;
                            }
                            printer.append("\ninvokevirtual " + cnode.type + "/");
                        } else {
                            Node pnode = new Node();
                            for (Expression e : dot.expressions) {
                                pnode = e.accept(this);
                                b &= pnode.res;
                            }
                            printer.append("\ninvokestatic ");
                        }
                        num_code_generated++;
                        printer.append(dot.id.varID);
                        printer.append("(");
                        symbolTable.put(fn.id.varID, fn.returnType.type);
                        if (f.parameterList.parameters.size() == dot.expressions.size()) {
                            if (f.parameterList.parameters.size() == 0) {
                                undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                if (!functionStack.peek().getKey().equals(fn.id.varID))
                                    functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                symbolTable.put(fn.id.varID, fn.returnType.type);
                            } else {
                                for (VarDecl v : fn.parameterList.parameters) {
                                    printer.append(getStringType(v.type.type));
                                }
                            }
                            for (int i = 0; i < f.parameterList.parameters.size(); i++) {
                                undoStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                if (!functionStack.peek().getKey().equals(fn.id.varID))
                                    functionStack.push(new Pair<String, String>(fn.id.varID, fn.returnType.type));
                                symbolTable.put(fn.id.varID, fn.returnType.type);
                            }
                        }
                        num_code_generated++;
                        break;
                    }
                }
                cnode.type = classMap.get(cnode.type).parentClass;
            }
            if (symbolTable.containsKey(dot.id.varID)) {
                functionStack.pop();
            }

            Node fnode = dot.id.accept(this);
            printer.append(")" + getStringType(fnode.type));
            b &= cnode.res;
            b &= fnode.res;
            node.type = fnode.type;
            if(node.type.equals("BOOLEAN"))
                node.type = "INT";
            node.res &= b;
            node.value = "";
            num_code_generated++;
            num_code_generated++;
        }
        return node;
    }

    @Override
    public Node visit(Block n) {
        Node node = new Node();
        for (Statement s : n.body) {
            Node snode = s.accept(this);
        }
        return node;
    }

    @Override
    public Node visit(Skip n) {
        return null;
    }

    private void copyParentFieldsIntoSymbolTable(Class v, Map<String, Boolean> visitedClass) {
        if (visitedClass.containsKey(v.id.varID) && visitedClass.get(v.id.varID)) {
            return;
        }
        visitedClass.put(v.id.varID, true);
        if (classMemberVariable.containsKey(v.parentClass)) {
            for (Pair<String, String> p : classMemberVariable.get(v.parentClass)) {
                undoStack.push(p);
                symbolTable.put(p.getKey(), p.getValue());
            }
            copyParentFieldsIntoSymbolTable(classMap.get(v.parentClass), visitedClass);
        }
    }

    private Boolean checkIfVariableOverriding(String varID, Class clazz, Map<String, Boolean> visitedClass) {
        if (visitedClass.containsKey(clazz.id.varID) && visitedClass.get(clazz.id.varID)) {
            // it is a circular inheritance my friend
            System.err.println("It does not make any sense to check overriding variables inside a circular inheritance graph");
            return true;
        } else {
            visitedClass.put(clazz.id.varID, true);
            // check if a variable is defined in the parent class
            if (classMemberVariable.containsKey(clazz.parentClass)) {
                String parent = clazz.parentClass;
                for (Pair<String, String> pair : classMemberVariable.get(parent)) {
                    String id = pair.getKey();
                    if (id.equals(varID)) {
                        return true;
                    }
                }
                if (classMap.containsKey(clazz.parentClass)) {
                    return checkIfVariableOverriding(varID, classMap.get(clazz.parentClass), visitedClass);
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    private Boolean checkIfFunctionOverridingWithDifferentArgs(Function function, Class clazz, Map<String, Boolean> visitedClass) {
        if (visitedClass.containsKey(clazz.id.varID) && visitedClass.get(clazz.id.varID)) {
            // it is a circular inheritance my friend
            System.err.println("It does not make any sense to check overriding functions inside a circular inheritance graph");
            return true;
        } else {
            visitedClass.put(clazz.id.varID, true);
            // check if a function is having different arguments
            if (classMemberfunction.containsKey(clazz.parentClass)) {
                String parent = clazz.parentClass;
                for (Function f : classMemberfunction.get(parent)) {
                    String id = f.id.varID;
                    if (id.equals(function.id.varID)) {
                        Boolean b = true;
                        if (!function.returnType.type.equals("INT") && !function.returnType.type.equals("INT-ARRAY") &&
                                !function.returnType.type.equals("STRING") && !function.returnType.type.equals("BOOLEAN") &&
                                !f.returnType.type.equals("INT") && !f.returnType.type.equals("INT-ARRAY") &&
                                !f.returnType.type.equals("STRING") && !f.returnType.type.equals("BOOLEAN")) {
                            if (!isSubtype(function.returnType.type, f.returnType.type)) {
                                b &= true;
                                System.err.println(function.getLine() + 1 + ":" + (function.getCol() + 1) + " error: return type of: '" + clazz.id.varID + " :: " + function.id.varID + "' and '" + parent + " :: " + f.id.varID + "' do not match");
                            }
                        }
                        if (f.parameterList.parameters.size() == function.parameterList.parameters.size()) {
                            for (int i = 0; i < f.parameterList.parameters.size(); i++) {
                                VarDecl varDecl = f.parameterList.parameters.get(i);
                                if (!varDecl.type.type.equals(function.parameterList.parameters.get(i).type.type)) {
                                    if (isSubtype(function.parameterList.parameters.get(i).type.type, varDecl.type.type)) {
                                        return false;
                                    }
                                    return true;
                                }
                            }
                            return false;
                        }
                        return true;
                    }
                }
                if (classMap.containsKey(clazz.parentClass)) {
                    return checkIfVariableOverriding(function.id.varID, classMap.get(clazz.parentClass), visitedClass);
                } else {
                    return false;
                }
            }
            return false;
        }
    }

    public boolean isSubtype(String subclass, String superclass) {
        boolean b = false;
        if ((classMap.containsKey(subclass) && classMap.containsKey(superclass))) {
            while (!classMap.get(subclass).parentClass.equals("")) {
                if (classMap.get(subclass).parentClass.equals(superclass)) {
                    b = true;
                    return b;
                }
                subclass = classMap.get(subclass).parentClass;
            }
        }
        return b;
    }

    @Override
    public Node visit(Class v) {
        //check for class
        if (isClassVisited) {
            local_var_counter = 0;
            enterScope();
            try {
                String cclass = "" + v.id.varID + ".j";
                int index = files.indexOf(cclass);
                writer = new FileWriter(files.get(index));
                printer = new PrintWriter(writer, true);
                printer.write("");
                printer.flush();
                printer.append(".class public " + v.id.varID);
                if (!v.parentClass.equals("")) {
                    printer.append("\n.super " + v.parentClass);
                } else {
                    printer.append("\n.super java/lang/Object");
                }
                for (VarDecl varDecl : v.varDecls) {
                    if (isClassVisited)
                        varDecl.accept(this);
                }
                if(v.parentClass.equals("")){
                    printer.append("\n.method public <init>()V");
                    printer.append("\naload_0");
                    printer.append("\ninvokespecial java/lang/Object/<init>()V");
                    printer.append("\nreturn");
                    printer.append("\n.end method");
                }else{
                    printer.append("\n.method public <init>()V");
                    printer.append("\naload_0");
                    printer.append("\ninvokespecial "+v.parentClass+"/<init>()V");
                    printer.append("\nreturn");
                    printer.append("\n.end method");
                }
                for (Function function : v.functions) {
                    functionStack.push(new Pair<String, String>(function.id.varID, function.returnType.type));
                    //for double pass through all the functions of all the classes
                    if (isClassVisited) {
                        function.accept(this);
                    }
                }
                printer.close();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (!classStack.empty())
                classStack.pop();
            exitScope();
        }

        //this is for checking circular graph inheritance
        Map<String, Boolean> visitedClass = new HashMap<>();
        for (String s : classSet) {
            visitedClass.put(s, false);
        }
        enterScope();
        //copy the all the variables from the parent class or its grand parents
        for (String s : classSet) {
            visitedClass.put(s, false);
        }
        copyParentFieldsIntoSymbolTable(v, visitedClass);
        for (VarDecl varDecl : v.varDecls) {
            //This is for adding variables of corresponding class
            if (symbolTable.containsKey(varDecl.var.varID)) {

            } else {
                if (classMemberVariable.containsKey(v.id.varID)) {
                    classMemberVariable.get(v.id.varID).add(new Pair(varDecl.var.varID, varDecl.type.type));
                }
                // what if the current variable is defined in the parent (grand pa) class
                for (String s : visitedClass.keySet()) {
                    visitedClass.put(s, false);
                }
                boolean overriding = checkIfVariableOverriding(varDecl.var.varID, v, visitedClass);
                if (overriding) {

                }
            }
//            varDecl.accept(this);
        }
        for (Function function : v.functions) {
            if (symbolTable.containsKey(function.id.varID)) {
            }
            if (classMemberfunction.containsKey(v.id.varID)) {
                classMemberfunction.get(v.id.varID).add(function);
            }
            for (String s : classSet) {
                visitedClass.put(s, false);
            }
            if (checkIfFunctionOverridingWithDifferentArgs(function, v, visitedClass)) {
            }
            undoStack.push(new Pair<String, String>(function.id.varID, function.returnType.type));
            symbolTable.put(function.id.varID, function.returnType.type);
        }
        for (Function function : v.functions) {
            functionMemberVariable.put(function.id.varID, new ArrayList<String>());
//            functionStack.push(new Pair<String, String>(function.id.varID, function.returnType.type));
            //for double pass through all the functions of all the classes
//            if (isClassVisited)
//                function.accept(this);
        }
        if (!classStack.empty())
            classStack.pop();
        exitScope();

        return null;
    }

    @Override
    public Node visit(Type t) {
        return null;
    }

    @Override
    public Node visit(This t) {
        Node n = new Node();
        if (isClassVisited) {
            n.type = classStack.peek();
        }
        return n;
    }

    @Override
    public Node visit(Plus n) {
        //root
        Node node = new Node();
        if (isPrintLn) {
            printer.append("\nnew java/lang/StringBuilder");
            printer.append("\ndup");

            printer.append("\ninvokespecial java/lang/StringBuilder/<init>()V");
            //lhs
            Node lnode = new Node();
            lnode = n.lhs.accept(this);
            printer.append("\ninvokevirtual java/lang/StringBuilder.append("+getStringType(lnode.type)+")Ljava/lang/StringBuilder;");
            //rhs
            Node rnode = new Node();
            rnode = n.rhs.accept(this);
            printer.append("\ninvokevirtual java/lang/StringBuilder.append("+getStringType(rnode.type)+")Ljava/lang/StringBuilder;");
            //tostring
            printer.append("\ninvokevirtual java/lang/StringBuilder.toString()Ljava/lang/String;");
            node.type = rnode.type;
            node.value = "" + lnode.value + rnode.value;
            num_code_generated++;
        } else {
            Node lnode = new Node();
            lnode = n.lhs.accept(this);
            //rhs
            Node rnode = new Node();
            rnode = n.rhs.accept(this);
            if (symbolTable.containsKey(lnode.value))
                getOrLoad(lnode);
            if (symbolTable.containsKey(rnode.value))
                getOrLoad(rnode);
            node.type = lnode.type;
            printer.append("\n" + getPrefix(node.type) + "add");
            num_code_generated++;
        }
        return node;
    }

    @Override
    public Node visit(Not n) {
        Node node = new Node();
        //lhs
        Node lnode = n.expr.accept(this);
        getOrLoad(lnode);
        //root
        node.type = "BOOLEAN";
        node.value = "\nif_" + getPrefix(lnode.type) + "ne";
        return node;
    }

    @Override
    public Node visit(Neg n) {
        Node node = new Node();
        //lhs
        Node lnode = n.expr.accept(this);
        getOrLoad(lnode);
        //root
        node.type = "BOOLEAN";
        node.value = "\nif_" + getPrefix(lnode.type) + "neg";
        return node;
    }

    @Override
    public Node visit(For n) {
        return null;
    }

    @Override
    public Node visit(And n) {
        //lhs
        Node lnode = new Node();
        lnode = n.lhs.accept(this);
        if (symbolTable.containsKey(lnode.value))
            getOrLoad(lnode);
        //rhs
        Node rnode = new Node();
        rnode = n.rhs.accept(this);
        if (symbolTable.containsKey(rnode.value))
            getOrLoad(rnode);
        //root
        printer.append("\n" + getPrefix(lnode.type) + "and");
        return null;
    }

    @Override
    public Node visit(Or n) {
        //lhs
        Node lnode = new Node();
        lnode = n.lhs.accept(this);
        if (symbolTable.containsKey(lnode.value))
            getOrLoad(lnode);
        //rhs
        Node rnode = new Node();
        rnode = n.rhs.accept(this);
        if (symbolTable.containsKey(rnode.value))
            getOrLoad(rnode);
        //root
        printer.append("\n" + getPrefix(lnode.type) + "or");
        return null;
    }

    @Override
    public Node visit(Var n) {
        Node node = new Node();
        if (isClassVisited && !functionStack.isEmpty()) {
            if (symbolTable.containsKey(n.varID)) {
                node.value = n.varID;
                node.type = symbolTable.get(n.varID);
            }
        }
        return node;
    }
}
